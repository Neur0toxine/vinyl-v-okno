<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersConfirmationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersConfirmationsTable Test Case
 */
class UsersConfirmationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersConfirmationsTable
     */
    public $UsersConfirmations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_confirmations',
        'app.users',
        'app.cart',
        'app.orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('UsersConfirmations') ? [] : ['className' => UsersConfirmationsTable::class];
        $this->UsersConfirmations = TableRegistry::get('UsersConfirmations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersConfirmations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
