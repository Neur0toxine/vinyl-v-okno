<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $name
 * @property string $year
 * @property int $rating
 * @property $authors
 * @property $tracks
 * @property float $price
 * @property int $bought
 * @property \Cake\I18n\FrozenTime $last_bought
 *
 * @property \App\Model\Entity\Category[] $categories
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'year' => true,
        'rating' => true,
        'rating_count' => true,
        'authors' => true,
        'tracks' => true,
        'price' => true,
        'bought' => true,
        'last_bought' => true,
        'img_big' => true,
        'img_small' => true,
        'categories' => true
    ];
}
