<?php
 $this->layout = false;
 $this->extend('/Layout/default');
 $preTitle = 'Активация'
?>
<?= $this->element('MainMenu'); ?>
<div class="row p-4">
    <div class="col-12 p-0 text-center">
      <h3 class="w-100"><?= $activate_msg ?></h3>
      <p class="lead w-100"><?= $prepend_msg ?></p>
    </div>
</div>