<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category[]|\Cake\Collection\CollectionInterface $categories
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Html->link(__('Новый жанр'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Список продуктов'), ['controller' => 'Products', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Новый продукт'), ['controller' => 'Products', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="categories index large-9 medium-8 columns content">
    <h3><?= __('Жанры') ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name', 'Имя') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= $this->Number->format($category->id) ?></td>
                <td><?= h($category->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Просмотр'), ['action' => 'view', $category->id]) ?>
                    <?= $this->Html->link(__('Править'), ['action' => 'edit', $category->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $category->id], ['confirm' => __('Вы действительно хотите удалить этот элемент #{0}?', $category->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('В начало')) ?>
            <?= $this->Paginator->prev('< ' . __('Назад')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
            <?= $this->Paginator->last(__('В конец') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Страница {{page}} из {{pages}}, показано {{current}} записей из {{count}} всего')]) ?></p>
    </div>
</div>
