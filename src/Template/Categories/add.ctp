<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Html->link(__('Список жанров'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Список пластинок'), ['controller' => 'Products', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Новая пластинка'), ['controller' => 'Products', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="editor-form my-2 mx-auto">
    <?= $this->Form->create($category) ?>
    <div class="form-group">
        <?
            echo $this->Form->label('name', 'Имя');
            echo $this->Form->text('name', [
                'class' => 'form-control',
                'placeholder' => 'Введите имя',
                'id' => 'name'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('productsids', 'Добавить пластинки в этот жанр');
            echo $this->Form->control('products._ids', ['options' => $products, 'class' => 'form-control']);
        ?>
    </div>
    <?= $this->Form->button(__('Добавить'), ['class' => 'btn btn-secondary']) ?>
    <?= $this->Form->end() ?>
</div>
