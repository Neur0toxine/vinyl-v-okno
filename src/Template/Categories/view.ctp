<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-row justify-content-center">
        <div class="btn-group btn-sm d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Html->link(__('Править жанр'), ['action' => 'edit', $category->id], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Form->postLink(__('Удалить жанр'), ['action' => 'delete', $category->id], ['confirm' => __('Вы уверены, что хотите удалить этот элемент #{0}?', $category->id), 'class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список жанров'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Новый жанр'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список продуктов'), ['controller' => 'Products', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Новый продукт'), ['controller' => 'Products', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
        </div>
</div>
<div class="col-12 mt-2">
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <tr>
            <th scope="row"><?= __('ID') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Имя') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('В этом жанре') ?></h4>
        <?php if (!empty($category->products)): ?>
        <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
            <tr>
                <th scope="col"><?= __('ID') ?></th>
                <th scope="col"><?= __('Название') ?></th>
                <th scope="col"><?= __('Год выпуска') ?></th>
                <th scope="col"><?= __('Рейтинг') ?></th>
                <th scope="col"><?= __('Цена') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
            <?php foreach ($category->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->name) ?></td>
                <td><?= h($products->year) ?></td>
                <td><?= h($products->rating) ?></td>
                <td><?= h($products->price) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Просмотр'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Правка'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Вы действительно хотите удалить этот элемент #{0}?', $products->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
