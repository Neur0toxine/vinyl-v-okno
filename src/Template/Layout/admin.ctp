<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Core\Configure;
?>
<!DOCTYPE html>
<html class="no-js" lang="ru">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>
        <?
        if(isset($title)) echo $title;
        else echo Configure::read('title');
        ?>
    </title>

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="/site.webmanifest">
    <link rel="apple-touch-icon" href="/icon.png">

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('normalize.css') ?>
    <!-- <link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet"> -->
    <?= $this->Html->css('pacifico-webfont.css') ?>
    <?= $this->Html->css('admin.css') ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css">
</head>
<body>
        <div id="app" class="container">
            <!--[if lte IE 9]>
                <p class="browserupgrade">Вы используете <strong>устаревший</strong> браузер. Пожалуйста, <a href="https://browsehappy.com/" target="_blank">обновите Ваш браузер</a> для корректной работы.</p>
            <![endif]-->
            <?= $this->Flash->render() ?>
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="mainmenu">
                            <a href="/users/index" class="navbar-brand p-0"><div id="main-brand">
                                <img src="/img/vinyl-record.svg" alt="Логотип сайта">
                                <span><? if(isset($menutitle)) echo $menutitle; else echo Configure::read('title');?></span>
                            </div></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav mr-auto">
                            <li class="nav-item">
                                <a class="nav-link" href="/users/index">Пользователи</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/cart/index">Корзины</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/orders/index">Заказы</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/products/index">Товары</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/categories/index">Жанры</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/feedback/index">Обратная связь</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/">Выход</a>
                            </li>
                            </ul>
                        </div>
                        </nav>
            <script>var selected=document.querySelector('a.nav-link[href="'+window.location.pathname+'"]');selected.className=selected.className+" active";</script>
            <?= $this->fetch('content') ?>
        </div>
        <div id="tooltips-container">

        </div>
        <script src="/js/vendor/modernizr-3.5.0.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
        <script>window.jQuery || document.write('<script src="/js/vendor/jquery-3.2.1.min.js"><\/script>')</script>
        <script src="https://unpkg.com/popper.js@1.12.9/dist/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        <script src="/js/plugins.js"></script>
        <script src="/js/queryParser.js"></script>
        <script src="/js/admin.js"></script>

        <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<!--         <script>
            window.ga=function(){ga.q.push(arguments)};ga.q=[];ga.l=+new Date;
            ga('create','UA-XXXXX-Y','auto');ga('send','pageview')
        </script>
        <script src="https://www.google-analytics.com/analytics.js" async defer></script> -->
</body>
</html>