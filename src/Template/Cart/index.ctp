<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cart[]|\Cake\Collection\CollectionInterface $cart
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
 use Cake\ORM\TableRegistry;
 $ut = TableRegistry::get('Users');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <? $this->Html->link(__('New Cart'), ['action' => 'add']) ?>
        <?= $this->Html->link(__('Список пользователей'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Новый пользователь'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-12">
<div class="cart index large-9 medium-8 columns content">
    <h3><?= __('Корзины') ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id', 'Пользователь') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contents', 'Содержимое') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cart as $cart): ?>
            <tr>
                <td><?= $this->Number->format($cart->id) ?></td>
                <td><?= $cart->has('user') ? $this->Html->link('[' . $cart->user->id . ']: ' . 
                    $ut->find()->select(['username'])->where(['id' => $cart->user->id])->first()['username'], ['controller' => 'Users', 'action' => 'view', $cart->user->id]) : '' ?></td>
                <td>
                    <? foreach($cart->contents as $prod) {
                       echo $this->Html->link('Пластинка №' . $prod['id'] . ', ' . $prod['count'] . ' шт.', ['controller' => 'Products', 'action' => 'view', $prod['id']]) . '<br>';
                    }
                    ?>

                </td>
                <td class="actions list-actions">
                    <?= $this->Html->link(__('Просмотр'), ['action' => 'view', $cart->id]) ?>
                    <?= $this->Html->link(__('Правка'), ['action' => 'edit', $cart->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $cart->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $cart->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('В начало')) ?>
            <?= $this->Paginator->prev('< ' . __('Назад')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
            <?= $this->Paginator->last(__('В конец') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Страница {{page}} из {{pages}}, показано {{current}} записей из {{count}} всего')]) ?></p>
    </div>
</div>
</div>
</div>