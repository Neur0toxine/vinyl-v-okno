<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;
$pageTitle = 'Винил в окно';
$fullprice = 0;
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xl-10">
        <?
        foreach($products as $e):
            echo $this->element('CartProduct', [
            'id' => $e['product']->id,
            'name' => $e['product']->name,
            'poster' => $e['product']->img_small,
            'year' => $e['product']->year,
            'rating' => $e['product']->rating,
            'count' => $e['count'],
            'price' => $e['product']->price]);
            $fullprice += $e['product']->price * $e['count'];
        endforeach;
        ?>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 col-xl-2 p-2 d-flex flex-column align-items-center cart-sidebar">
        <p class="lead mb-1">Итого:</p>
        <h3 class="mb-3" id="fullprice"><?= $fullprice ?> ₽</h3>
        <button class="btn btn-pastel-red w-auto mb-1" onclick="confirm('Оформить заказ?')&&window.location.assign('/orders/create')">Оформить заказ</button>
        <button class="btn btn-pastel-red w-auto" onclick="App.fromCart('all')">Очистить корзину</button>
    </div>
</div>