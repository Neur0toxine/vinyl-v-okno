<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cart $cart
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
        <div class="col-12 d-flex flex-direction-row justify-content-center">
            <div class="btn-group mb-2 d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Form->postLink(
                __('Удалить'),
                ['action' => 'delete', $cart->id],
                ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $cart->id), 'class' => 'btn btn-secondary']
            )
        ?>
            <?= $this->Html->link(__('Список корзин'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Список пользователей'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Новый пользователь'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
<div class="col-12">
        <div class="cart editor-form large-9 medium-8 columns content my-2 mx-auto">
            <?= $this->Form->create($cart) ?>
            <div class="form-group">
                <?
                    echo $this->Form->label('user_idx', 'ID владельца корзины');
                    echo $this->Form->control('user_id', ['options' => $users, 'class' => 'form-control']);
                ?>
            </div>
            <?= $this->Form->text('contents', ['class' => 'hidden']); ?>
            <div class="form-group">
                <?= $this->Form->label('contents_fallback', 'Содержимое'); ?>
                <div class="d-flex flex-row">
                    <input type="text" class="form-control" id="cart_fallback_text" placeholder="Введите ID пластинки" data-toggle="tooltip" data-placement="top" title="ID пластинки можно взять из списка пластинок">
                    <input type="number" min="1" id="cart_cnt" class="form-control ml-1" data-toggle="tooltip" data-placement="top" title="Это количество добавляемого в корзину товара">
                    <button onclick="App.addToCart(event);" class="btn btn-secondary mr-1 ml-1">
                        Добавить
                    </button>
                    <button onclick="App.removeFromCart(event);" class="btn btn-secondary mr-1 ml-1">
                        Удалить
                    </button>
                </div>
                <select class="form-control mt-1" id="cart_fallback" onclick="App.jsonFromCartItemText()" multiple></select>
            </div>
            <?= $this->Form->button(__('Сохранить'), ['class' => 'btn btn-secondary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>