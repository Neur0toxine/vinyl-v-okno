<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Cart $cart
 */
 $this->layout = false;
 $this->extend('/Layout/admin');

  use Cake\ORM\TableRegistry;
  $ut = TableRegistry::get('Users');
  $pt = TableRegistry::get('Products');
?>
<?= $this->Html->css('main.css') ?>
<?= $this->Html->css('admin.css') ?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-row justify-content-center">
        <div class="btn-group btn-sm d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Html->link(__('Редактировать корзину'), ['action' => 'edit', $cart->id], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Form->postLink(__('Удалить корзину'), ['action' => 'delete', $cart->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $cart->id), 'class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список корзин'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <? $this->Html->link(__('New Cart'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список пользователей'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Новый пользователь'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
    <div class="col-12">
        <div class="cart view large-9 medium-8 columns content">
            <h3><?= 'ID корзины: ' . h($cart->id) ?></h3>
            <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
                <tr>
                    <th scope="row"><?= __('Пользователь') ?></th>
                    <td><?= $cart->has('user') ? $this->Html->link($cart->user->id, ['controller' => 'Users', 'action' => 'view', $cart->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Содержимое') ?></th>
                    <td class="d-flex flex-row flex-wrap">
                        <? 
                        $cart_contents = $cart->contents;
                        if(gettype($cart_contents) == "string")
                            $cart_contents = json_decode($cart->contents);
                        if(isset($cart_contents))
                            foreach($cart_contents as $item)
                            {
                                $product = $pt
                                    ->find()
                                    ->select(['name', 'img_small', 'id', 'rating'])
                                    ->where(['id' => $item['id']])
                                    ->first();
                                echo '<div class="d-flex flex-column">';
                                echo $this->element('ProductCard', ['title' => $product->name, 'price' => $product->price, 'img' => $product->img_small, 'alt' => 'Обложка', 'id' => $product->id, 'rating' => $product->rating, 'ro' => true]);
                                echo '<p class="ml-auto mr-auto mt-0">' . $item['count'] . ' ед.</p></div>';
                            }
                        ?>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>