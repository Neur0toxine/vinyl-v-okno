<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;
$pageTitle = 'Винил в окно';
$products = new ProductsController();
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row">
    <div class="col-12">
        <? foreach($orders as $order): ?>
        <div class="order p-2 mx-auto d-flex flex-column align-items-center justify-content-center">
            <h4>Заказ №<?= $order->id ?></h4>
            <table class="table table-border table-responsive-md">
                <tr>
                    <th scope="row">Состояние</th>
                    <td><?= $order->state ?></td>
                </tr>
                <tr>
                    <th scope="row">Трек-номер</th>
                    <td><?= $order->track ?></td>
                </tr>
                <tr>
                    <th scope="row">Содержимое</th>
                    <td>
                        <div class="d-flex flex-row flex-wrap">
                        <? 
                            foreach($order->contents['data'] as $d): 
                            $product = $products->Products->get($d['id']);
                        ?>
                            <a href="/products/show/<?= $product->id ?>" target="_blank" style="display:inline-block;">
                                <span class="badge badge-my1"><?= $product->name ?>, <?= $d['price'] ?> руб / шт, <?= $d['count'] ?> шт.</span>
                            </a>
                        <? endforeach; ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Стоимость</th>
                    <td>
                    <?=
                     $order->contents['costs'];
                    ?>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Создан</th>
                    <td><?= $order->timestamp; ?></td>
                </tr>
            </table>
        </div>
        <? endforeach ?>
    </div>
    <div class="col-12">
        <div class="paginator mt-2 d-flex flex-row justify-content-center">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('В начало')) ?>
                <?= $this->Paginator->prev('< ' . __('Назад')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
                <?= $this->Paginator->last(__('В конец') . ' >>') ?>
            </ul>
        </div>
    </div>
</div>