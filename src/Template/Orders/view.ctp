<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
 $this->layout = false;
 $this->extend('/Layout/admin');

use Cake\ORM\TableRegistry;
 $ut = TableRegistry::get('Users');
 $pt = TableRegistry::get('Products');
?>
<?= $this->Html->css('main.css') ?>
<?= $this->Html->css('admin.css') ?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-row justify-content-center">
        <div class="btn-group btn-sm d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Html->link(__('Редактировать заказ'), ['action' => 'edit', $order->id], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Form->postLink(__('Удалить заказ'), ['action' => 'delete', $order->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $order->id), 'class' => 'btn btn-secondary'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список заказов'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <? $this->Html->link(__('Новый заказ'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список пользователей'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Новый пользователь'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
<div class="col-12">
<div class="orders view large-9 medium-8 columns content">
    <h3><?= 'ID заказа: ' . h($order->id) ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover">
        <tr>
            <th scope="row"><?= __('Пользователь') ?></th>
            <td><?= $order->has('user') ? $this->Html->link('(' . $order->user->id . '): ' . 
                    $ut->find()->select(['username'])->where(['id' => $order->user->id])->first()['username'], ['controller' => 'Users', 'action' => 'view', $order->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Состояние') ?></th>
            <td><?= $order->state ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Трек-номер') ?></th>
            <td><?= $order->track ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Содержимое') ?></th>
            <td class="d-flex flex-row flex-wrap">
                <? 
                if(isset($order->contents['data']))
                    foreach($order->contents['data'] as $item)
                    {
                        $product = $pt
                            ->find()
                            ->select(['name', 'price', 'img_small', 'id', 'rating'])
                            ->where(['id' => $item['id']])
                            ->first();
                        echo '<div class="d-flex flex-column">';
                        echo $this->element('ProductCard', ['title' => $product->name, 'price' => $item['price'], 'img' => $product->img_small, 'alt' => 'Обложка отсутствует', 'id' => $product->id, 'rating' => $product->rating, 'ro' => true]);
                        echo '<p class="ml-auto mr-auto mt-0">' . $item['count'] . ' ед.</p></div>';
                    }
                ?>
            </td>
        </tr>
    </table>
</div>
</div>
</div>