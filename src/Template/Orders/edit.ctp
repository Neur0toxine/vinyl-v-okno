<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
        <div class="col-12 d-flex flex-direction-row justify-content-center">
            <div class="btn-group mb-2 d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
            <?= $this->Form->postLink(
                    __('Удалить'),
                    ['action' => 'delete', $order->id],
                    ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $order->id),
                    'class' => 'btn btn-secondary']
                )
            ?>
            <?= $this->Html->link(__('Список заказов'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Список пользователей'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Новый пользователь'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
<div class="col-12">
    <div class="orders editor-form my-2 mx-auto large-9 medium-8 columns content">
            <?= $this->Form->create($order) ?>
            <div class="form-group">
                <?
                    echo $this->Form->label('user_idx', 'ID владельца заказа');
                    echo $this->Form->control('user_id', ['options' => $users, 'class' => 'form-control']);
                ?>
            </div>
            <div class="form-group">
                <?
                    echo $this->Form->label('track', 'Трек-номер');
                    echo $this->Form->text('track', ['class' => 'form-control']);
                ?>
            </div>
            <div class="form-group">
                <?
                echo $this->Form->label('state', 'Состояние заказа');
                echo $this->Form->select('state', [
                'Обрабатывается' => 'Обрабатывается',
                'В пути' => 'В пути',
                'Доставлен' => 'Доставлен',
                'Утерян на почте' => 'Утерян на почте'
                ], ['class' => 'form-control', 'value' => $order->state]); ?>
            </div>
            <?= $this->Form->text('contents', ['class' => 'hidden']); ?>
            <div class="form-group">
                <?= $this->Form->label('contents_fallback', 'Содержимое'); ?>
                <div class="d-flex flex-row">
                    <input type="text" class="form-control" id="order_fallback_text" placeholder="Введите ID пластинки" data-toggle="tooltip" data-placement="top" title="ID пластинки можно взять из списка пластинок">
                    <input type="number" min="1" id="order_cnt" class="form-control ml-1" data-toggle="tooltip" data-placement="top" title="Это количество добавляемого в заказ товара">
                     <input type="number" min="1" id="order_price" class="form-control ml-1" data-toggle="tooltip" data-placement="top" title="Это цена за единицу добавляемого в заказ товара">
                    <button onclick="App.addToOrder(event);" class="btn btn-secondary mr-1 ml-1">
                        Добавить
                    </button>
                    <button onclick="App.removeFromOrder(event);" class="btn btn-secondary mr-1 ml-1">
                        Удалить
                    </button>
                </div>
                <select class="form-control mt-1" id="order_fallback" onclick="App.jsonFromOrderItemText()" multiple></select>
            </div>
            <p id="itog"></p>
            <?= $this->Form->button(__('Сохранить'), ['class' => 'btn btn-secondary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>