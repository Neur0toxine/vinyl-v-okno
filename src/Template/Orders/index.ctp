<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
 use Cake\ORM\TableRegistry;
 $ut = TableRegistry::get('Users');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <? $this->Html->link(__('New Order'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Список пользователей'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Новый пользователь'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-12">
<div class="orders index large-9 medium-8 columns content">
    <h3><?= __('Заказы') ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                <th scope="col"><?= $this->Paginator->sort('user_id', 'Пользователь') ?></th>
                <th scope="col"><?= $this->Paginator->sort('track', 'Трек №') ?></th>
                <th scope="col"><?= $this->Paginator->sort('state', 'Состояние') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contents', 'Содержимое') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $order): ?>
            <tr>
                <td><?= $this->Number->format($order->id) ?></td>
                <td><?= $order->has('user') ? $this->Html->link('[' . $order->user->id . ']: ' . 
                    $ut->find()->select(['username'])->where(['id' => $order->user->id])->first()['username'], ['controller' => 'Users', 'action' => 'view', $order->user->id]) : '' ?></td>
                <td><?= $order->track ?></td>
                <td><?= $order->state ?></td>
                <td>
                    <?
                        echo '<p class="mb-0"><b>Пластинки</b></p>';
                        if(isset($order->contents['data']))
                            foreach($order->contents['data'] as $item)
                                echo $this->Html->link(__('Пластинка №' . $item['id'] . ', цена: ' . $item['price'] . ' руб.'),
                                ['controller' => 'Products', 'action' => 'view', $item['id']]) . '<br>';
                        if(isset($order->contents['costs']))
                            echo '<p><b>Итого:</b> ' . $order->contents['costs'] . ' руб.</p>';
                    ?>
                </td>
                <td class="actions list-actions">
                    <?= $this->Html->link(__('Просмотр'), ['action' => 'view', $order->id]) ?>
                    <?= $this->Html->link(__('Правка'), ['action' => 'edit', $order->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $order->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $order->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('В начало')) ?>
            <?= $this->Paginator->prev('< ' . __('Назад')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
            <?= $this->Paginator->last(__('В конец') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Страница {{page}} из {{pages}}, показано {{current}} записей из {{count}} всего')]) ?></p>
    </div>
</div>
</div>
</div>