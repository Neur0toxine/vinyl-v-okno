<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;
$this->set('title', 'О сайте - ' . Configure::read('title'));
?>
<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row">
	<div class="col-12">
		<div class="w-100 h-100">
			<div class="d-flex flex-column align-items-center justify-content-center" style="max-width:40rem;margin: 0 auto;">
				<img class="big-poster" src="/img/hellothere.jpg" alt="Здрасьте!">
				<p class="text-center mt-2 mb-1">Вы на сайте Интернет-магазина виниловых пластинок "Винил в окно". Сайт сделан в рамках курсовой работы тремя студентами: <a href="https://vk.com/neur0xt" target="_blank">этим</a>, <a href="https://vk.com/id147940652" target="_blank">этим</a>, и <a href="https://vk.com/id109884907" target="_blank">вот этим</a>.</p>
				<p class="text-center mb-1">Главное - не пытайтесь тут что-нибудь заказать, это всего лишь курсовая работа :)</p>
				<p class="text-center mb-2">Но если вас заинтересовал сайт - милости просим стучаться ВКонтакте.</p>
			</div>
		</div>
	</div>
</div>