<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;

$pctrl = new ProductsController();
$products = $pctrl->display(
        ['id', 'name', 'rating', 'price', 'img_small'],
        ['rating >=' => 4.5],
        ['last_bought' => 'DESC'],
        1, 10
    );
$pageTitle = 'Винил в окно';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row">
    <div class="col-12">
        <div id="jumbo-main" class="jumbotron" style="margin-top: .4rem">
        <h1 class="display-3 text-white text-shadow-black">Добро пожаловать!</h1>
        <p class="lead text-white text-shadow-black">Для поиска нужной пластинки перейдите на страницу поиска.</p>
        <hr class="my-4">
        <p class="lead text-white text-shadow-black">
            <a class="btn btn-danger btn-lg" href="/products/navigator" role="button">Перейти</a>
        </p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12"><h2 class="text-center">Наиболее популярные пластинки</h2></div>
    <hr class="my-4 text-primary text-shadow-black">
</div>
<div class="row d-flex flex-row justify-content-center align-items-start align-content-start align-self-start flex-wrap p-4">
<?
foreach ($products as $row) {
    echo $this->element('ProductCard', ['title' => $row->name, 'price' => $row->price, 'img' => $row->img_small, 'alt' => 'Обложка', 'id' => $row->id, 'rating' => $row->rating]);
}
?>
</div>