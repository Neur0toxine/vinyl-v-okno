<?php
$this->layout = false;
$preTitle = 'Оферта';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row p-4">
	<div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-xl-6 p-0">
		<form method="post">
		  <div class="form-group">
		    <label for="InputEmail">Адрес E-Mail</label>
		    <input type="email" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Введите E-Mail">
		    <small id="emailHelp" class="form-text text-muted">Отправляя эту форму вы соглашаетесь с нашей политикой конфиденциальности</small>
		  </div>
		  <div class="form-group">
		    <label for="text">Текст обращения</label>
		    <textarea class="form-control" id="text" rows="3"></textarea>
		  </div>
		  <button type="submit" class="btn btn-pastel-red">Отправить</button>
		</form>
	</div>
</div>