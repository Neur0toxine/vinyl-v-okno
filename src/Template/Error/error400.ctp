<?php
use Cake\Core\Configure;
use Cake\Error\Debugger;

$this->layout = false;

if (Configure::read('debug')) :
    $this->layout = 'dev_error';

    $this->assign('title', $message);
    $this->assign('templateName', 'error400.ctp');

    $this->start('file');
?>
<?php if (!empty($error->queryString)) : ?>
    <p class="notice">
        <strong>SQL Query: </strong>
        <?= h($error->queryString) ?>
    </p>
<?php endif; ?>
<?php if (!empty($error->params)) : ?>
        <strong>SQL Query Params: </strong>
        <?php Debugger::dump($error->params) ?>
<?php endif; ?>
<?= $this->element('auto_table_warning') ?>
<?php
if (extension_loaded('xdebug')) :
    xdebug_print_function_stack();
endif;

$this->end();
endif;
?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>И... её нет.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>

        * {
            line-height: 1.2;
            margin: 0;
        }

        html {
            color: #888;
            display: table;
            font-family: sans-serif;
            height: 100%;
            text-align: center;
            width: 100%;
        }

        body {
            display: table-cell;
            vertical-align: middle;
            margin: 2em auto;
        }

        h1 {
            color: white;
            text-shadow: 1px 1px 10px black, -1px -1px 20px black, 2px 2px 4px black;
            font-size: 2em;
            font-weight: 400;
        }

        p {
            margin: 0 auto;
            color: white;
            text-shadow: 1px 1px 10px black, -1px -1px 20px black, 2px 2px 4px black;
            /*width: 280px;*/
        }

        a:link, a:visited {
            color: #d0d6e6;
        }

        #top-image {
            background: url(/img/deathstar.jpg) -25px -50px;
            position:fixed ;
            top: 0;
            width:100%;
            z-index:-99;
              height:100%;
              background-size: calc(100% + 50px);
        }

        #dots {
            background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABZJREFUeNpi2r9//38gYGAEESAAEGAAasgJOgzOKCoAAAAASUVORK5CYII=);
             width:100%;
             height: 100%;
             z-index: -98;
            position: fixed;
            top: 0;
            opacity: .1;
        }

        @media only screen and (max-width: 280px) {

            body, p {
                width: 95%;
            }

            h1 {
                font-size: 1.5em;
                margin: 0 0 0.3em;
            }

        }

    </style>
</head>
<body>
    <div id="top-image"></div>
    <div id="dots"></div>
    <div id="container">
        <h1>Хм, похоже такой страницы нет!</h1>
        <p>Зато есть Звезда Смерти.</p>
        <p>Может быть, стоит <a href="/">вернуться на главную</a>?</p>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
        var movementStrength = 10;
        var height = movementStrength / $(window).height();
        var width = movementStrength / $(window).width();
        $("#dots, #container").mousemove(function(e){
                  var pageX = e.pageX - ($(window).width() / 2);
                  var pageY = e.pageY - ($(window).height() / 2);
                  var newvalueX = width * pageX * -1 - 25;
                  var newvalueY = height * pageY * -1 - 50;
                  $('#top-image').css("background-position", newvalueX+"px     "+newvalueY+"px");
        });
        });
    </script>
</body>
</html>
<!-- IE needs 512+ bytes: https://blogs.msdn.microsoft.com/ieinternals/2010/08/18/friendly-http-error-pages/ -->
