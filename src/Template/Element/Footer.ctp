<footer id="footer" class="clearfix">
<div id="footer-widgets">

  <div class="container">

  <div id="footer-wrapper">

    <div class="row">
      <div class="col-sm-12 col-md-4">
        <div id="meta-3" class="widget widgetFooter widget_meta">
        <h4 class="widgettitle">Важные страницы :</h4>
        <ul>
        <li><a href="/"><i class="fa fa-home fa-fw"></i>&nbsp;Главная</a></li>
        <li><a href="/pages/oferta"><i class="fa fa-link"></i>&nbsp;&nbsp;Оферта</a></li>
        <li><a href="/pages/feedback"><i class="fa fa-envelope fa-fw"></i>&nbsp;Обратная связь</a></li>
       </ul>
</div>      </div> <!-- end widget1 -->

      <div class="col-sm-12 col-md-4">
                <div id="recent-posts-3" class="widget widgetFooter widget_recent_entries">
              <h4 class="widgettitle">Мы в соцсетях:</h4>
              <ul>
              <li>
               <a href="#" target="_blank"><i class="fa fa-vk"></i>&nbsp;Вконтакте</a>
              </li>
              <li>
                <a href="#" target="_blank"><i class="fa fa-facebook"></i>&nbsp;&nbsp;Twitter</a>
              </li>
              <li>
                <a href="#" target="_blank"><i class="fa fa-twitter"></i>&nbsp;Facebook</a>
             </li>
          </ul>
  </div>            </div> <!-- end widget1 -->


        </div>
       </div> <!-- end widget1 -->

  </div> <!-- end #footer-wrapper -->

  </div> <!-- end .container -->
  <div id="sub-floor">
  <div class="container">
    <div class="row">
      <div class="col-md-4 copyright">
       Copyright Винил в окно © 2017
      </div>
      <div class="col-md-4 col-md-offset-4 attribution text-center text-muted">
          Создано командой слоупоков!
      </div>
    </div> <!-- end .row -->
  </div>
</div> <!-- end #footer-widgets -->

</footer>