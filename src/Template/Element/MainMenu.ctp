<?
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
use App\Controller\UsersController;

$users = new UsersController();
$username = $users->getUserName($users->getUserId());
if($username == '') $username = 'Пользователь';

$genres = TableRegistry::get('Categories');
$query = $genres->find()->order(['id' => 'ASC']);
?>
<nav class="navbar navbar-expand-lg navbar-dark" id="mainmenu">
            <a href="/" class="navbar-brand">
                <div id="main-brand">
                    <img src="/img/vinyl-record.svg" alt="Логотип сайта">
                    <span><? if(isset($menutitle)) echo $menutitle; else echo Configure::read('title');?></span>
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/products/navigator">Каталог</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Жанры
                    </a>
                     <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <? foreach($query as $genre): ?>
                        <a class="dropdown-item" href="/products/navigator?genres=[<?= $genre['id'] ?>]"><?= h($genre['name']) ?></a>
                        <? endforeach; ?>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/feedback/add">Обратная связь</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/pages/about">О сайте</a>
                </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" id="main-menu-searchinput" type="search" placeholder="Поиск" aria-label="Поиск">
                <button class="btn my-2 my-sm-0 btn-pastel-red" id="main-menu-searchbtn" type="submit">Поиск</button>
                </form>
                <ul class="navbar-nav my-2 my-lg-0 ml-auto">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i><?= h($username) ?></i>
                        </a>
                         <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <? if(!$users->getUserId()): ?>
                                <a class="dropdown-item" href="/users/login">Вход</a>
                                <a class="dropdown-item" href="/users/register">Регистрация</a>
                                <a class="dropdown-item" href="/users/resetpassword">Сброс пароля</a>
                            <? else: ?>
                                <? if($users->isAdmin()): ?>
                                    <a class="dropdown-item" href="/users/index">Администрирование</a>
                                <? endif; ?>
                                    <a class="dropdown-item" href="/users/profile">Профиль</a>
                                    <a class="dropdown-item" href="/cart/show">Корзина</a>
                                    <a class="dropdown-item" href="/orders/show">Заказы</a>
                                    <a class="dropdown-item" href="/users/logout">Выход</a>
                            <? endif; ?>
                        </div>
                    </li>
                </ul>
            </div>
            </nav>
<script>var selected=document.querySelector('a.nav-link[href="'+window.location.pathname+'"]');selected.className=selected.className+" active";</script>