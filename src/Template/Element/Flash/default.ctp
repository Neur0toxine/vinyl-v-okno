<?php
$class = 'alert alert-primary alert-top';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="<?= h($class) ?>" role="alert" onclick="$(this).slideUp(300);">
	<?= $message ?>
</div>