<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-danger alert-top" role="alert" onclick="$(this).slideUp(300);">
	<?= $message ?>
</div>