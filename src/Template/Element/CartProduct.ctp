<?
use Cake\ORM\TableRegistry;
?>
<div class="cart-product mx-2 my-2 d-flex align-items-center align-self-center flex-row flex-wrap" id="product<?= $id ?>">
  <div class="poster p-2 d-flex flex-row align-items-center justify-content-center align-self-center w-100">
    <img src="<?= $poster ?>" alt="Обложка альбома">
  </div>
  <div class="d-flex flex-column align-items-center align-self-center justify-content-center mr-1" style="flex-grow:1;">
    <div class="w-100 h-auto p-1">
      <table class="table table-sm">
        <tbody>
          <tr>
            <th scope="row">Название</th>
            <td><?= $name ?></td>
          </tr>
          <tr>
            <th scope="row">Год</th>
            <td><?= $year ?> год.</td>
          </tr>
          <tr>
            <th scope="row">Рейтинг</th>
            <td><?= $rating ?></td>
          </tr>
          <tr>
            <th scope="row">Количество</th>
            <td><?= $count ?></td>
          </tr>
          <tr>
            <th scope="row">Цена</th>
            <td><?= $price ?> ₽</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="w-auto p-2 d-flex flex-row align-items-center justify-content-center align-self-center">
      <p class="my-1 mr-2" id="counter<?= $id ?>"><?= $count ?> шт.</p>
      <div class="btn-group my-1 mr-1" role="group" aria-label="Кнопки добавления и удаления пластинки">
        <button type="button" class="btn btn-sm btn-pastel-red" onclick="App.toCart(<?= $id ?>, 1)">+</button>
        <button type="button" class="btn btn-sm btn-pastel-red" onclick="App.fromCart(<?= $id ?>, 1)">-</button>
      </div>
      <button class="btn btn-sm btn-pastel-red my-1" onclick="App.fromCart(<?= $id ?>)">Удалить из корзины</button>
    </div>
  </div>
</div>