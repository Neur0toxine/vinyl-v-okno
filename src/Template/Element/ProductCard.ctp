<?
use Cake\ORM\TableRegistry;
use App\Controller\UsersController;

$users = new UsersController();
$rd = isset($ro) ? ' disabled' : ($users->getUserId() == 0 ? ' disabled' : '');
$fl = $users->getUserId() == 0 ? ',false' : ',true';
?>
    <div class="vinyl-box">
        <a href="/products/show/<?= $id ?>" target="_blank"><img class="vinyl-box-card" src="<?= $img ?>" alt="<?= $alt ?>"></a>
        <div class="vinyl-box-heading"><span><?= $title ?></span></div>
        <div class="vinyl-box-price"><span><?= $price ?> руб.</span></div>
        <div class="vinyl-box-rating">
            <form action=""<? if(!$users->isLoggedIn()) echo ' onclick="App.tooltip(\'Это действие доступно только зарегистрированным пользователям.\')"' ?>>
              <input class="star star-5" id="star-5-<?= $id ?>" type="radio" name="star<?= $id ?>" onclick="App.changeRating(<?= $id ?>,5<?= $fl ?>);"<?= $rd ?> <? if(round($rating) == 5): echo 'checked'; endif;?>/>
              <label class="star star-5" for="star-5-<?= $id ?>"></label>
              <input class="star star-4" id="star-4-<?= $id ?>" type="radio" name="star<?= $id ?>" onclick="App.changeRating(<?= $id ?>,4<?= $fl ?>);"<?= $rd ?> <? if(round($rating) == 4): echo 'checked'; endif;?>/>
              <label class="star star-4" for="star-4-<?= $id ?>"></label>
              <input class="star star-3" id="star-3-<?= $id ?>" type="radio" name="star<?= $id ?>" onclick="App.changeRating(<?= $id ?>,3<?= $fl ?>);"<?= $rd ?> <? if(round($rating) == 3): echo 'checked'; endif;?>/>
              <label class="star star-3" for="star-3-<?= $id ?>"></label>
              <input class="star star-2" id="star-2-<?= $id ?>" type="radio" name="star<?= $id ?>" onclick="App.changeRating(<?= $id ?>,2<?= $fl ?>);"<?= $rd ?> <? if(round($rating) == 2): echo 'checked'; endif;?>/>
              <label class="star star-2" for="star-2-<?= $id ?>"></label>
              <input class="star star-1" id="star-1-<?= $id ?>" type="radio" name="star<?= $id ?>" onclick="App.changeRating(<?= $id ?>,1<?= $fl ?>);"<?= $rd ?> <? if(round($rating) == 1): echo 'checked'; endif;?>/>
              <label class="star star-1" for="star-1-<?= $id ?>"></label>
            </form>
        </div>
        <div class="vinyl-box-order">
            <button class="btn btn-pastel-red btn-sm" onclick="App.toCart(<?= $id ?>)">В корзину</button>
        </div>
    </div>