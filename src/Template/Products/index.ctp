<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product[]|\Cake\Collection\CollectionInterface $products
 */
 $this->layout = false;
$this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
         <?= $this->Html->link(__('Новая пластинка'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?>
         <?= $this->Html->link(__('Список жанров'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
         <?= $this->Html->link(__('Новый жанр'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-12">
<div class="products index large-9 medium-8 columns content">
    <h3><?= __('Пластинки') ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name', 'Имя') ?></th>
                <th scope="col"><?= $this->Paginator->sort('year', 'Год') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rating', 'Рейтинг') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rating_count', 'Оценок') ?></th>
                <th scope="col"><?= $this->Paginator->sort('authors', 'Авторы') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tracks', 'Записи') ?></th>
                <th scope="col"><?= $this->Paginator->sort('price', 'Стоимость') ?></th>
                <th scope="col"><?= $this->Paginator->sort('bought', 'Куплено') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_bought', 'Последний раз') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($products as $product): ?>
            <tr>
                <td><?= $this->Number->format($product->id) ?></td>
                <td><?= h($product->name) ?></td>
                <td><?= h($product->year) ?></td>
                <td><?= $this->Number->format($product->rating) ?></td>
                <td><?= $this->Number->format($product->rating_count) ?></td>
                <td>
                    <? 
                        $pra = $product->authors;
                        if(gettype($pra) == "string")
                            $pra = json_decode($product->authors);
                    ?>
                    <? foreach($pra as $author): ?>
                    <p class="m-0"><?= h($author) ?></p>
                    <? endforeach; ?>
                </td>
                <td>
                    <? 
                        $prt = $product->tracks;
                        if(gettype($prt) == "string")
                            $prt = json_decode($product->tracks);
                    ?>
                    <? foreach($prt as $track): ?>
                    <p class="m-0"><?= h($track) ?></p>
                    <? endforeach; ?>
                </td>
                <td><?= $this->Number->format($product->price) ?></td>
                <td><?= $this->Number->format($product->bought) ?></td>
                <td><?= h($product->last_bought) ?></td>
                <td class="actions list-actions">
                    <?= $this->Html->link(__('Просмотр'), ['action' => 'view', $product->id]) ?>
                    <?= $this->Html->link(__('Править'), ['action' => 'edit', $product->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $product->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $product->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('В начало')) ?>
            <?= $this->Paginator->prev('< ' . __('Назад')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
            <?= $this->Paginator->last(__('В конец') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Страница {{page}} из {{pages}}, показано {{current}} записей из {{count}} всего')]) ?></p>
    </div>
</div>
</div>
</div>
