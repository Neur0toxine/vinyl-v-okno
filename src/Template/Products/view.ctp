<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<?= $this->Html->css('main.css') ?>
<?= $this->Html->css('admin.css') ?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-row justify-content-center">
        <div class="btn-group btn-sm d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Html->link(__('Править пластинку'), ['action' => 'edit', $product->id], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Form->postLink(__('Удалить пластинку'), ['action' => 'delete', $product->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $product->id), 'class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список пластинок'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Новая пластинка'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Жанры'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Новый жанр'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="row mt-2 mb-2">
    <div class="col-md-4 col-12 mb-1">
        <div class="imageContainerFF">
            <?= $this->element('ProductCard', ['title' => $product->name, 'price' => $product->price, 'img' => $product->img_small, 'alt' => 'Обложка', 'id' => $product->id, 'rating' => $product->rating, 'ro' => true]); ?>
        </div>
    </div>
    <div class="col-md-4 col-12 mb-1">
        <div class="imageContainerFF">
            <img id="selectedImageBig" src="<?= $product->img_big ?>" alt="Большая обложка">
        </div>
    </div>
    <div class="col-md-4 col-12">
        <div class="imageContainerFF">
            <img id="selectedImageSmall" src="<?= $product->img_small ?>" alt="Малая обложка">
        </div>
    </div>
</div>
<div class="row">
<div class="col-12">
    <h3><?= h($product->name) ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover">
        <tr>
            <th scope="row"><?= __('Имя') ?></th>
            <td><?= h($product->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Год') ?></th>
            <td><?= h($product->year) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Авторы') ?></th>
            <td>
                <?
                    $product_authors = $product->authors;
                    if(gettype($product_authors) == "string")
                        $product_authors = json_decode($product->authors);
                ?>
                <? foreach($product_authors as $author): ?>
                <p class="m-0"><?= $author ?></p>
                <? endforeach; ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Треки') ?></th>
            <td>
                <?
                    $product_tracks = $product->tracks;
                    if(gettype($product_tracks) == "string")
                        $product_tracks = json_decode($product->tracks);
                ?>
                <? foreach($product_tracks as $track): ?>
                <p class="m-0"><?= $track ?></p>
                <? endforeach; ?>
            </td>
        </tr>
        <tr>
            <th scope="row"><?= __('Оценка') ?></th>
            <td><?= $this->Number->format($product->rating) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Кол-во оценок') ?></th>
            <td><?= $this->Number->format($product->rating_count) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Цена') ?></th>
            <td><?= $this->Number->format($product->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Куплено раз') ?></th>
            <td><?= $this->Number->format($product->bought) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Дата последней покупки') ?></th>
            <td><?= h($product->last_bought) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Жанры этой пластинки') ?></h4>
        <?php if (!empty($product->categories)): ?>
        <table class="table table-bordered table-striped table-sm table-hover">
            <tr>
                <th scope="col"><?= __('Имя') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
            <?php foreach ($product->categories as $categories): ?>
            <tr>
                <td><?= h($categories->name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Просмотр'), ['controller' => 'Categories', 'action' => 'view', $categories->id]) ?>
                    <?= $this->Html->link(__('Правка'), ['controller' => 'Categories', 'action' => 'edit', $categories->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['controller' => 'Categories', 'action' => 'delete', $categories->id], ['confirm' => __('Это удалит сам жанр, чтобы убрать жанр из списка жанров этой пластинки - отредактируйте её. Продолжить?', $categories->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>