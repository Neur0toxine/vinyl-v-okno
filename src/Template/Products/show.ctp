<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\UsersController;

$this->layout = false;
$pageTitle = 'Винил в окно';
$users = new UsersController();
$rd = isset($ro) ? ' disabled' : ($users->getUserId() == 0 ? ' disabled' : '');
$fl = $users->getUserId() == 0 ? ',false' : ',true';
$product_authors = $product->authors;
if(gettype($product_authors) == "string")
    $product_authors = json_decode($product->authors);
$product_tracks = $product->tracks;
if(gettype($product_tracks) == "string")
    $product_tracks = json_decode($product->tracks);
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row">
    <div class="d-flex flex-row align-items-center justify-content-center flex-wrap flex-xl-nowrap flex-sm-wrap flex-xs-wrap flex-md-wrap flex-lg-wrap col-12">
          <div class="poster-big p-4 d-flex flex-row align-items-center justify-content-center align-self-center">
            <img src="<?= $product->img_big ?>" alt="Обложка альбома" class="big-poster">
          </div>
          <div class="d-flex flex-column align-items-center align-self-center justify-content-center px-2" style="flex-grow:1;">
            <div class="h-auto p-1" style="flex-grow:1;">
              <table class="table table-sm">
                <tbody>
                  <tr>
                    <th scope="row">Название</th>
                    <td><?= $product->name ?></td>
                  </tr>
                  <tr>
                    <th scope="row">Год</th>
                    <td><?= $product->year ?> год.</td>
                  </tr>
                  <tr>
                    <th scope="row">Рейтинг</th>
                    <td>
                        <div class="vinyl-box-rating">
                            <form action=""<? if(!$users->isLoggedIn()) echo ' onclick="App.tooltip(\'Это действие доступно только зарегистрированным пользователям.\')"' ?>>
                              <input class="star star-5" id="star-5-<?= $product->id ?>" type="radio" name="star<?= $product->id ?>" onclick="App.changeRating(<?= $product->id ?>,5<?= $fl ?>);"<?= $rd ?> <? if(round($product->rating) == 5): echo 'checked'; endif;?>/>
                              <label class="star star-5" for="star-5-<?= $product->id ?>"></label>
                              <input class="star star-4" id="star-4-<?= $product->id ?>" type="radio" name="star<?= $product->id ?>" onclick="App.changeRating(<?= $product->id ?>,4<?= $fl ?>);"<?= $rd ?> <? if(round($product->rating) == 4): echo 'checked'; endif;?>/>
                              <label class="star star-4" for="star-4-<?= $product->id ?>"></label>
                              <input class="star star-3" id="star-3-<?= $product->id ?>" type="radio" name="star<?= $product->id ?>" onclick="App.changeRating(<?= $product->id ?>,3<?= $fl ?>);"<?= $rd ?> <? if(round($product->rating) == 3): echo 'checked'; endif;?>/>
                              <label class="star star-3" for="star-3-<?= $product->id ?>"></label>
                              <input class="star star-2" id="star-2-<?= $product->id ?>" type="radio" name="star<?= $product->id ?>" onclick="App.changeRating(<?= $product->id ?>,2<?= $fl ?>);"<?= $rd ?> <? if(round($product->rating) == 2): echo 'checked'; endif;?>/>
                              <label class="star star-2" for="star-2-<?= $product->id ?>"></label>
                              <input class="star star-1" id="star-1-<?= $product->id ?>" type="radio" name="star<?= $product->id ?>" onclick="App.changeRating(<?= $product->id ?>,1<?= $fl ?>);"<?= $rd ?> <? if(round($product->rating) == 1): echo 'checked'; endif;?>/>
                              <label class="star star-1" for="star-1-<?= $product->id ?>"></label>
                            </form>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Оценок</th>
                    <td><?= $product->rating_count ?> шт.</td>
                  </tr>
                  <tr>
                    <th scope="row">Куплено</th>
                    <td><?= $product->bought ?> раз</td>
                  </tr>
                  <tr>
                    <th scope="row">Авторы</th>
                    <td>
                        <div class="d-flex flex-row align-items-center flex-wrap">
                            <? foreach($product_authors as $a): ?>
                                <span class="badge badge-my1"><?= h($a); ?></span>
                            <? endforeach; ?>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Жанры</th>
                    <td>
                       <div class="d-flex flex-row align-items-center flex-wrap">
                            <? foreach($product->categories as $categories): ?>
                                <span class="badge badge-my1"><?= h($categories->name); ?></span>
                            <? endforeach; ?>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Композиции</th>
                    <td>
                       <div class="d-flex flex-row align-items-center flex-wrap">
                            <? foreach($product_tracks as $a): ?>
                                <span class="badge badge-my1"><?= h($a); ?></span>
                            <? endforeach; ?>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <th scope="row">Цена</th>
                    <td><?= $product->price ?> ₽</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="d-flex flex-row flex-wrap align-items-center justify-content-center">
                <button class="btn btn-pastel-red w-auto mb-1 mx-1" onclick="App.toCart(<?= $product->id ?>)">Добавить в корзину</button>
                <div class="dropdown">
                  <button class="btn btn-pastel-red dropdown-toggle mb-1 mx-1" type="button" id="productActions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Убрать из корзины
                  </button>
                  <div class="dropdown-menu" aria-labelledby="productActions">
                    <a class="dropdown-item" href="#" onclick="App.fromCart(<?= $product->id ?>,1)">Убрать одну</a>
                    <a class="dropdown-item" href="#" onclick="App.fromCart(<?= $product->id ?>)">Убрать все</a>
                  </div>
                </div>
                <button class="btn btn-pastel-red w-auto mb-1 mx-1" onclick="App.fromCart('all')">Очистить корзину</button>
            </div>
        </div>
    </div>
</div>