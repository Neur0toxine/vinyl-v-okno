<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
 $this->layout = false;
 $this->extend('/Layout/admin');

 $imgBig = isset($product->id) ? $product->img_big : '';
 $imgSmall = isset($product->id) ? $product->img_small : '';
?>
<link rel="stylesheet" href="/css/cropper.min.css">
<script src="/js/cropper.min.js"></script>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
        <?= $this->Form->postLink(
                        __('Удалить'),
                        ['action' => 'delete', $product->id],
                        ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $product->id),
                        'class' => 'btn btn-secondary']
                    )
                ?>
        <?= $this->Html->link(__('Список пластинок'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Список жанров'), ['controller' => 'Categories', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
        <?= $this->Html->link(__('Создать жанр'), ['controller' => 'Categories', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="editor-form my-2 mx-auto">
    <?= $this->Form->create($product, ['name' => 'productsform']) ?>
     <?
         $this->Form->unlockField('photo'); 
         $this->Form->unlockField('photo.error'); 
         $this->Form->unlockField('photo.name'); 
         $this->Form->unlockField('photo.size'); 
         $this->Form->unlockField('photo.tmp_name'); 
         $this->Form->unlockField('photo.type'); 
     ?>
    <div class="form-group">
        <?
            echo $this->Form->label('name', 'Имя');
            echo $this->Form->text('name', [
                'class' => 'form-control',
                'placeholder' => 'Введите имя',
                'id' => 'name'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('year', 'Год выпуска');
            echo $this->Form->number('year', [
                'class' => 'form-control',
                'placeholder' => '',
                'id' => 'year',
                'min' => '1970',
                'max' => '9999'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('rating', 'Рейтинг');
            echo $this->Form->number('rating', [
                'class' => 'form-control',
                'min' => '1',
                'max' => '5',
                'id' => 'rating'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('rating_count', 'Количество оценок');
            echo $this->Form->number('rating_count', [
                'class' => 'form-control',
                'min' => '1',
                'id' => 'rating_count'
                ]);
        ?>
    </div>

    <div class="d-flex flex-row align-items-center justify-content-center mt-2 mb-2">
        <div class="container flex p-0 m-0">
            <div class="row p-0 m-0">
                <div class="col-md-4 col-12 p-0 m-0 mb-2">
                    <div class="imageContainerFF">
                        <img id="selectedImageSmall" src="<?= $imgSmall ?>"></img>
                    </div>
                </div>
                <div class="col-md-8 col-12 p-0 m-0 mb-2">
                    <div class="imageContainerFF">
                        <img id="selectedImageBig" src="<?= $imgBig ?>"></img>
                    </div>
                </div>
            </div>
            <div class="row mt-2">
                <div class="col-12">
                    <div class="d-flex flex-column align-items-center justify-content-center">
                        <button type="button" class="btn btn-primary" onclick="event.preventDefault()" data-toggle="modal" data-target="#exampleModal">
                          Изменить картинку
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Вставка картинки</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="container flex">
                    <div class="row">
                        <div class="col-12 p-0 m-0" id="imgContainer">
                            <img id="product-image" style="max-width:100%!important;" src="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <input type="file" class="form-control" id="picField" size="24" style="min-width:100%" alt=""/>
                        </div>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="App.acceptImg(event)" data-dismiss="modal">Выбрать картинку</button>
                <button type="button" class="btn btn-secondary" onclick="event.preventDefault()" data-dismiss="modal">Закрыть</button>
              </div>
            </div>
          </div>
        </div>
    </div>

        <?= $this->Form->text('authors', ['style' => 'display:none', 'id' => 'authors']); ?>
    <div class="form-group">
        <?= $this->Form->label('authors_fallback', 'Авторы'); ?>
        <div class="d-flex flex-row">
            <input type="text" class="form-control" id="authors_fallback_text">
            <button onclick="App.addToList('#authors_fallback_text', '#authors_fallback', event);" class="btn btn-secondary mr-1 ml-1">
                Добавить
            </button>
            <button onclick="App.removeFromList('#authors_fallback_text', '#authors_fallback', event);" class="btn btn-secondary mr-1 ml-1">
                Удалить
            </button>
        </div>
        <select class="form-control mt-1" id="authors_fallback" onclick="App.textFromSelected('#authors_fallback', '#authors_fallback_text')" multiple></select>
    </div>

        <?= $this->Form->text('tracks', ['style' => 'display:none', 'id' => 'tracks']); ?>
    <div class="form-group">
        <?= $this->Form->label('tracks_fallback', 'Треки'); ?>
        <div class="d-flex flex-row">
            <input type="text" class="form-control" id="tracks_fallback_text">
            <button onclick="App.addToList('#tracks_fallback_text', '#tracks_fallback', event);" class="btn btn-secondary mr-1 ml-1">
                Добавить
            </button>
            <button onclick="App.removeFromList('#tracks_fallback_text', '#tracks_fallback', event);" class="btn btn-secondary mr-1 ml-1">
                Удалить
            </button>
        </div>
        <select class="form-control mt-1" id="tracks_fallback" onclick="App.textFromSelected('#tracks_fallback', '#tracks_fallback_text')" multiple></select>
    </div>

    <div class="form-group">
        <?
            echo $this->Form->label('price', 'Цена');
            echo $this->Form->number('price', [
                'class' => 'form-control',
                'min' => '0',
                'id' => 'price'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('bought', 'Сколько куплено');
            echo $this->Form->number('bought', [
                'class' => 'form-control',
                'min' => '1',
                'id' => 'bought'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('last_bought', 'Дата последней покупки', ['class' => 'mr-2']);
            echo $this->Form->datetime('last_bought', [
                'minYear' => date('Y') - 5,
                'maxYear' => date('Y'),
                'class' => 'form-control'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('categories', 'Жанры');
            echo $this->Form->control('categories._ids', ['options' => $categories, 'class' => 'form-control']);
        ?>
    </div>
    <div class="mb-2 progress hidden">
      <div class="progress-bar progress-bar-striped progress-bar-animated" id="progr" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
    </div>
    <?= $this->Form->button(__('Сохранить'), ['class' => 'btn btn-secondary', 'onclick' => 'App.sendProductForm(event);']) ?>
    <?= $this->Form->end() ?>
</div>
