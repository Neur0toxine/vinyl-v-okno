<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;

$pageTitle = 'Винил в окно';
$preTitle = 'Поиск';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div class="row p-2 mx-2">
    <div class="col-xs-12 col-sm-6 col-md-5 col-lg-4 col-xl-3 mb-2">
      <div class="d-flex align-items-center justify-content-center justify-content-md-start justify-content-xl-start">
        <div id="accordion" role="tablist" class="w-100">
          <div class="card">
            <div class="card-header p-2" role="tab" id="headingOne">
              <p class="mb-0 text-center">
                <a class="accordion-link" data-toggle="collapse" href="#searchBar" aria-expanded="true" aria-controls="searchBar">
                  Фильтры
                </a>
              </p>
            </div>

            <div id="searchBar" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
              <div class="card-body p-1">
                <div class="d-flex flex-column align-items-center justify-content-center align-self-center">
                    <p class="mt-0 mb-1">Название</p>
                    <input type="text" class="form-control" placeholder="Введите название" id="vinylName" data-toggle="tooltip" data-placement="bottom" title="Введите название пластинки, или его часть">
                </div>
                    <div class="d-flex flex-column align-items-center justify-content-center align-self-center">
                        <p class="mt-2 mb-1">Год выпуска</p>
                        <div class="d-flex flex-row align-items-center justify-content-center m-0">
                            <input type="number" class="form-control" id="year_from" min="1500" max="<?= date('Y'); ?>" value="1970" data-toggle="tooltip" data-placement="right" title="Введите начальный год">
                            <input type="number" class="form-control" id="year_to" min="1500" max="<?= date('Y'); ?>" value="<?= date('Y'); ?>" data-toggle="tooltip" data-placement="right" title="Введите конечный год">
                        </div>
                    </div>
                    <div class="d-flex flex-column align-items-center justify-content-center align-self-center">
                        <p class="mt-2 mb-1">Рейтинг</p>
                        <form class="searchrating w-100 h-auto d-flex flex-row-reverse align-items-center justify-content-center align-self-center searchStarMin">
                          <input class="star star-5" id="star-5-searchBarMin" type="radio" value="5" name="starsearchBarMin">
                          <label class="star star-5" for="star-5-searchBarMin"></label>
                          <input class="star star-4" id="star-4-searchBarMin" type="radio" value="4" name="starsearchBarMin">
                          <label class="star star-4" for="star-4-searchBarMin"></label>
                          <input class="star star-3" id="star-3-searchBarMin" type="radio" value="3" name="starsearchBarMin">
                          <label class="star star-3" for="star-3-searchBarMin"></label>
                          <input class="star star-2" id="star-2-searchBarMin" type="radio" value="2" name="starsearchBarMin">
                          <label class="star star-2" for="star-2-searchBarMin"></label>
                          <input class="star star-1" id="star-1-searchBarMin" type="radio" value="1" name="starsearchBarMin" checked>
                          <label class="star star-1" for="star-1-searchBarMin"></label>
                          <p class="h-100 w-auto text-center mr-2">От: </p>
                        </form>
                        <form class="searchrating w-100 h-auto d-flex flex-row-reverse align-items-center justify-content-center align-self-center searchStarMax">
                          <input class="star star-5" id="star-5-searchBarMax" type="radio" value="5" name="starsearchBarMax" checked>
                          <label class="star star-5" for="star-5-searchBarMax"></label>
                          <input class="star star-4" id="star-4-searchBarMax" type="radio" value="4" name="starsearchBarMax">
                          <label class="star star-4" for="star-4-searchBarMax"></label>
                          <input class="star star-3" id="star-3-searchBarMax" type="radio" value="3" name="starsearchBarMax">
                          <label class="star star-3" for="star-3-searchBarMax"></label>
                          <input class="star star-2" id="star-2-searchBarMax" type="radio" value="2" name="starsearchBarMax">
                          <label class="star star-2" for="star-2-searchBarMax"></label>
                          <input class="star star-1" id="star-1-searchBarMax" type="radio" value="1" name="starsearchBarMax">
                          <label class="star star-1" for="star-1-searchBarMax"></label>
                          <p class="h-100 w-auto text-center mr-2">До: </p>
                        </form>
                    </div>
                    <div class="d-flex flex-column align-items-center justify-content-center">
                        <p class="text-center mt-2 w-100 h-auto mb-1">Минимум оценок</p>
                        <div class="d-flex flex-row align-items-center justify-content-center mx-2" style="width:90%" data-toggle="tooltip" data-placement="top" title="1" id="minrating_val">
                            <input id="minrating" type="text" value="1" class="w-100" data-slider-min="1" data-slider-max="100000" data-slider-step="5" data-slider-value="1"/>
                        </div>
                    </div>
                    <div class="d-flex flex-column align-items-center justify-content-center">
                        <p class="text-center mt-2 w-100 h-auto mb-1">Минимум покупок</p>
                        <div class="d-flex flex-row align-items-center justify-content-center mx-2" style="width:90%" data-toggle="tooltip" data-placement="top" title="1" id="minbought_val">
                            <input id="minbought" type="text" value="1" class="w-100" data-slider-min="1" data-slider-max="100000" data-slider-step="5" data-slider-value="1"/>
                        </div>
                    </div>
                    <div class="d-flex flex-column align-items-center justify-content-center">
                        <p class="text-center mt-2 w-100 h-auto mb-1">Цена</p>
                        <div class="d-flex flex-row align-items-center justify-content-center w-100 mb-1">
                            <div class="input-group">
                              <input type="text" class="form-control" id="slmin" min="10" max="40000" value="1000">
                              <span class="input-group-addon">₽</span>
                            </div>
                            <div class="input-group">
                              <input type="text" class="form-control" id="slmax" min="10" max="40000" value="10000">
                              <span class="input-group-addon">₽</span>
                            </div>
                        </div>
                        <div class="d-flex flex-row align-items-center justify-content-center mx-2" style="width:90%">
                            <input id="cost" type="text" value="" class="w-100" data-slider-min="10" data-slider-max="40000" data-slider-step="5" data-slider-value="[1000,10000]"/>
                        </div>
                    </div>
              </div>
            </div>
          </div>
          <div class="card">
            <div class="card-header p-2" role="tab" id="headingThree">
              <p class="mb-0 text-center">
                <a class="accordion-link collapsed" data-toggle="collapse" href="#genresBar" aria-expanded="false" aria-controls="genresBar">
                  Жанры
                </a>
              </p>
            </div>
            <div id="genresBar" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body p-1">
                <div class="d-flex flex-column align-items-center justify-content-center">
                  <p class="text-center mt-0 mb-1">Список жанров</p>
                  <div class="d-flex flex-row flex-wrap align-items-center justify-content-center align-self-center p-1 my-1 w-100" id="genreslist">
                    <div id="genres-list" class="well w-100" style="max-height: 300px;overflow: auto;">
                        <ul class="list-group checked-list-box">
                          <? foreach($categories as $cat): ?>
                          <li class="list-group-item" data-color="headred" data-value="<?= $cat->id ?>"><?= $cat->name ?></li>
                          <? endforeach; ?>
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
          <div class="card">
            <div class="card-header p-2" role="tab" id="headingTwo">
              <p class="mb-0 text-center">
                <a class="accordion-link collapsed" data-toggle="collapse" href="#authorsBar" aria-expanded="false" aria-controls="authorsBar">
                  Авторы
                </a>
              </p>
            </div>
            <div id="authorsBar" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
              <div class="card-body p-1">
                <div class="d-flex flex-column align-items-center justify-content-center">
                  <p class="text-center mt-0 mb-1">Список авторов</p>
                  <input type="text" class="form-control mb-0" id="authorname" placeholder="Введите автора">
                  <button class="btn btn-pastel-red w-100 mt-1" type="button" id="addauthorbtn">Добавить</button>
                  <div class="d-flex flex-row flex-wrap align-items-center justify-content-center align-self-center p-1 my-1 w-100" id="authorslist">
                  </div>
                </div>
              </div>
            </div>
        </div>
          <div class="card">
            <div class="card-header p-2" role="tab" id="headingThree">
              <p class="mb-0 text-center">
                <a class="accordion-link collapsed" data-toggle="collapse" href="#tracksBar" aria-expanded="false" aria-controls="tracksBar">
                  Композиции
                </a>
              </p>
            </div>
            <div id="tracksBar" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
              <div class="card-body p-1">
                <div class="d-flex flex-column align-items-center justify-content-center">
                  <p class="text-center mt-0 mb-1">Список композиций</p>
                  <input type="text" class="form-control mb-0" id="trackname" placeholder="Введите композицию">
                  <button class="btn btn-pastel-red w-100 mt-1" type="button" id="addtrackbtn">Добавить</button>
                  <div class="d-flex flex-row flex-wrap align-items-center justify-content-center align-self-center p-1 my-1 w-100" id="trackslist">
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
      </div>
      <button class="btn btn-pastel-red w-100 mx-auto mt-1" onclick="App.search()">Поиск</button>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8 col-xl-9 p-0">
        <div class="container flex d-flex flex-row justify-content-center justify-content-md-start justify-content-xl-start align-items-start align-content-start align-self-start flex-wrap p-0 mr-auto">
        <?
        foreach ($products as $row) {
        if($row != null)
            echo $this->element('ProductCard', ['title' => $row->name, 'price' => $row->price, 'img' => $row->img_small, 'alt' => 'Обложка', 'id' => $row->id, 'rating' => $row->rating]);
        }
        ?>
        </div>
        <div class="paginator mt-2 d-flex flex-row justify-content-center">
          <nav aria-label="Навигация по страницам">
            <ul class="pagination">
              <li class="page-item">
                <button class="page-link" href="#" onclick="App.jumpOverPage(-1)" aria-label="Назад">
                  <span aria-hidden="true">&laquo;</span>
                  <span class="sr-only">Назад</span>
                </button>
              </li>
              <li class="page-item">
                <button class="page-link" href="#" onclick="App.jumpOverPage(1)" aria-label="Вперёд">
                  <span aria-hidden="true">&raquo;</span>
                  <span class="sr-only">Вперёд</span>
                </button>
              </li>
            </ul>
          </nav>
        </div>
    </div>
</div>