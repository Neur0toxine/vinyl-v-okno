<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
            <?= $this->Html->link(__('Список пользователей'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Список корзин'), ['controller' => 'Cart', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <? $this->Html->link(__('Создать корзину'), ['controller' => 'Cart', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Список заказов'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <? $this->Html->link(__('Создать заказ'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="editor-form my-2 mx-auto">
    <?= $this->Form->create($user) ?>
    <div class="form-group">
        <?
            echo $this->Form->label('username', 'Имя пользователя');
            echo $this->Form->text('username', [
                'class' => 'form-control',
                'placeholder' => 'Введите имя пользователя',
                'id' => 'username'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('password', 'Пароль');
            echo $this->Form->text('password', [
                'class' => 'form-control',
                'placeholder' => 'Введите пароль',
                'id' => 'password'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('email', 'E-Mail');
            echo $this->Form->email('email', [
                'class' => 'form-control',
                'placeholder' => 'Введите E-Mail',
                'id' => 'email'
                ]);
        ?>
    </div>
    <div class="form-check form-check-admin">
        <?
            echo $this->Form->checkbox('role', [
                'id' => 'role'
                ]);
            echo $this->Form->label('role', 'Админ?', ['class' => 'form-check-label']);
        ?>
    </div>
    <div class="form-check form-check-admin">
        <?
            echo $this->Form->checkbox('active', [
                'id' => 'active'
                ]);
            echo $this->Form->label('active', 'Активация', ['class' => 'form-check-label']);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('firstname', 'Имя');
            echo $this->Form->text('firstname', [
                'class' => 'form-control',
                'placeholder' => 'Введите имя',
                'id' => 'firstname'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('lastname', 'Фамилия');
            echo $this->Form->text('lastname', [
                'class' => 'form-control',
                'placeholder' => 'Введите фамилию',
                'id' => 'lastname'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('middlename', 'Отчество');
            echo $this->Form->text('middlename', [
                'class' => 'form-control',
                'placeholder' => 'Введите отчество',
                'id' => 'middlename'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('address', 'Адрес');
            echo $this->Form->text('address', [
                'class' => 'form-control',
                'placeholder' => 'Введите адрес',
                'id' => 'address'
                ]);
        ?>
    </div>
    <div class="form-group">
        <?
            echo $this->Form->label('born', 'Дата рождения', ['class' => 'mr-2']);
            echo $this->Form->date('born', [
                'class' => 'form-control',
                'placeholder' => 'Введите дату рождения',
                'id' => 'born'
                ]);
        ?>
    </div>
    <?= $this->Form->button(__('Отправить'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Form->end() ?>
</div>
