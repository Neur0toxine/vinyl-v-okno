<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-direction-row justify-content-center">
        <div class="btn-group d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
            <?= $this->Html->link(__('Новый пользователь'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Список корзин'), ['controller' => 'Cart', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <? $this->Html->link(__('Создать корзину'), ['controller' => 'Cart', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
            <?= $this->Html->link(__('Список заказов'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?>
            <? $this->Html->link(__('Создать заказ'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
    </div>
</div>
<div class="row mt-2">
    <div class="col-12">
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Пользователи') ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
      <thead>
        <tr>
                <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                <th scope="col"><?= $this->Paginator->sort('username', 'Ник') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email', 'E-Mail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('role', 'Админ?') ?></th>
                <th scope="col"><?= $this->Paginator->sort('active', 'Активен?') ?></th>
                <th scope="col"><?= $this->Paginator->sort('firstname', 'Имя') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lastname', 'Фамилия') ?></th>
                <th scope="col"><?= $this->Paginator->sort('middlename', 'Отчество') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address', 'Адрес') ?></th>
                <th scope="col"><?= $this->Paginator->sort('born', 'Дата рождения') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
        </tr>
      </thead>
      <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td scope="row"><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= ($user->role == 1) ? __('Да') : __('Нет'); ?></td>
                <td><?= $user->active ? __('Да') : __('Нет'); ?></td>
                <td><?= h($user->firstname) ?></td>
                <td><?= h($user->lastname) ?></td>
                <td><?= h($user->middlename) ?></td>
                <td><?= h($user->address) ?></td>
                <td><?= h($user->born) ?></td>
                <td class="list-actions">
                    <?= $this->Html->link(__('Просмотр'), ['action' => 'view', $user->id]) ?>
                    <?= $this->Html->link(__('Править'), ['action' => 'edit', $user->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
      </tbody>
    </table>

    <nav aria-label="Навигация">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('К началу')) ?>
            <?= $this->Paginator->prev('< ' . __('Назад')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
            <?= $this->Paginator->last(__('К концу') . ' >>') ?>
        </ul>
    </nav>
    <p><?= $this->Paginator->counter(['format' => __('Страница {{page}} из {{pages}}, показано {{current}} записей из {{count}} всего')]) ?></p>
</div>
</div>
</div>