<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-column justify-content-center">
        <div class="btn-group btn-sm d-flex flex-row flex-wrap justify-content-center" role="group" aria-label="Действия">
            <?= $this->Html->link(__('Редактировать пользователя'), ['action' => 'edit', $user->id], ['class' => 'btn btn-secondary']) ?> 
            <?= $this->Form->postLink(__('Удалить пользователя'), ['action' => 'delete', $user->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $user->id), 'class' => 'btn btn-secondary']) ?> 
            <?= $this->Html->link(__('Список пользователей'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
            <?= $this->Html->link(__('Новый пользователь'), ['action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
        <!--</div>
        <div class="btn-group btn-sm" role="group" aria-label="Действия">-->
            <?= $this->Html->link(__('Список корзин'), ['controller' => 'Cart', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
            <? $this->Html->link(__('Новая корзина'), ['controller' => 'Cart', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?> 
            <?= $this->Html->link(__('Список заказов'), ['controller' => 'Orders', 'action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
            <? $this->Html->link(__('Новый заказ'), ['controller' => 'Orders', 'action' => 'add'], ['class' => 'btn btn-secondary']) ?>
        </div>
</div>
<div class="col-12">
    <h3>ID пользователя: <?= h($user->id) ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <tr>
            <th scope="row"><?= __('Ник') ?></th>
            <td><?= h($user->username) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('E-Mail') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Админ?') ?></th>
            <td><?= ($user->role == 1) ? __('Да') : __('Нет'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Имя') ?></th>
            <td><?= h($user->firstname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Фамилия') ?></th>
            <td><?= h($user->lastname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Отчество') ?></th>
            <td><?= h($user->middlename) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Адрес') ?></th>
            <td><?= $user->address ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Дата рождения') ?></th>
            <td><?= h($user->born) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Активен') ?></th>
            <td><?= $user->active ? __('Да') : __('Нет'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Корзина') ?></h4>
        <?php if (!empty($user->cart)): ?>
        <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
            <tr>
                <th scope="col"><?= __('ID') ?></th>
                <th scope="col"><?= __('Содержимое') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
            <?php foreach ($user->cart as $cart): ?>
            <tr>
                <td><?= h($cart->id) ?></td>
                <td><?= h($cart->user_id) ?></td>
                <td>
                    <?
                        $cart_contents = $cart->contents;
                        if(gettype($cart_contents) == "string")
                            $cart_contents = json_decode($cart->contents);
                    ?>
                    <? foreach($cart_contents as $prod) {
                       echo $this->Html->link('Пластинка №' . $prod['id'] . ', ' . $prod['count'] . ' шт.', ['controller' => 'Products', 'action' => 'view', $prod['id']]) . '<br>';
                    }
                    ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link(__('Просмотр'), ['controller' => 'Cart', 'action' => 'view', $cart->id]) ?>
                    <?= $this->Html->link(__('Правка'), ['controller' => 'Cart', 'action' => 'edit', $cart->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['controller' => 'Cart', 'action' => 'delete', $cart->id], ['confirm' => __('Действительно удалить этот элемент? #{0}?', $cart->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Заказы') ?></h4>
        <?php if (!empty($user->orders)): ?>
        <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
            <tr>
                <th scope="col"><?= __('ID') ?></th>
                <th scope="col"><?= __('Содержимое') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
            <?php foreach ($user->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->user_id) ?></td>
                <td>
                    <?
                        $order_contents = $orders->contents;
                        if(gettype($order_contents) == "string")
                            $order_contents = json_decode($order->contents);
                    ?>
                    <? foreach($order_contents['data'] as $prod) {
                       echo $this->Html->link('Пластинка №' . $prod['id'] . ', ' . $prod['count'] . ' шт.', ['controller' => 'Products', 'action' => 'view', $prod['id']]) . '<br>';
                    }
                    ?>
                </td>
                <td class="actions">
                    <?= $this->Html->link(__('Просмотр'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                    <?= $this->Html->link(__('Правка'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Действительно удалить этот элемент? #{0}?', $orders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
</div>