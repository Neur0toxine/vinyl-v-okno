<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;

$pageTitle = 'Винил в окно';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div style="max-width:27rem;margin:0 auto;">
<?= $this->Form->create() ?>
  <div class="form-group">
  	<?= $this->Form->label('email', 'E-Mail'); ?>
    <?= $this->Form->text('email', [
    		'type' => 'email',
	    	'class' => 'form-control',
	    	'id' => 'email',
	    	'placeholder' => 'Введите E-Mail',
        'onchange' => '$(\'#username\').val($(this).val().replace(/(\@.+)/, \'\').replace(/\W+/, \'_\'))'
    	]) ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('username', 'Имя пользователя'); ?>
    <?= $this->Form->text('username', [
        'type' => 'text',
        'class' => 'form-control',
        'id' => 'username',
        'placeholder' => 'Введите имя пользователя'
      ]) ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('password', 'Пароль'); ?>
    <?= $this->Form->text('password', [
    		'type' => 'password',
    		'class' => 'form-control',
    		'id' => 'password',
        'onchange' => 'if($(this).val() == $(\'#username\').val()) App.tooltip(\'Нельзя использовать никнейм как пароль!\')',
    		'placeholder' => 'Введите пароль'
    	]) ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('password_check', 'Повторите пароль'); ?>
    <input type="password" class="form-control" id="password_check" onchange="if($(this).val() != $('#password').val()) App.tooltip('Пароли должны совпадать!')" placeholder="Введите пароль">
  </div>
  <div class="form-group">
    <?= $this->Form->label('firstname', 'Ваше имя'); ?>
    <?= $this->Form->text('firstname', [
        'type' => 'text',
        'class' => 'form-control',
        'id' => 'firstname',
        'placeholder' => 'Введите ваше имя'
      ]) ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('lastname', 'Ваша фамилия'); ?>
    <?= $this->Form->text('lastname', [
        'type' => 'text',
        'class' => 'form-control',
        'id' => 'lastname',
        'placeholder' => 'Введите вашу фамилию'
      ]) ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('middlename', 'Ваше отчество'); ?>
    <?= $this->Form->text('middlename', [
        'type' => 'text',
        'class' => 'form-control',
        'id' => 'middlename',
        'placeholder' => 'Введите ваше отчество'
      ]) ?>
  </div>
  <div class="form-group">
    <label for="address">Ваш адрес</label>
    <?= $this->Form->textarea('address', ['class' => 'form-control', 'id' => 'address', 'rows' => '2', 'placeholder' => 'Введите сюда адрес']); ?>
  </div>
  <div class="form-group">
    <label for="born">Дата рождения</label>
    <?= $this->Form->text('born', ['class' => 'form-control', 'id' => 'born']); ?>
  </div>
  <?= $this->Form->button('Регистрация', ['class' => 'btn btn-pastel-red']) ?>
<?= $this->Form->end() ?>
</div>