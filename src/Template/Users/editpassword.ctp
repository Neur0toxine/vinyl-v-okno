<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;

$pageTitle = 'Винил в окно';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div style="max-width:27rem;margin:0 auto;">
<?= $this->Form->create() ?>
  <p class="lead">Смена пароля</p>
      <div class="form-group">
        <?
            echo $this->Form->label('old_password', 'Старый пароль');
            echo $this->Form->password('old_password', [
                'class' => 'form-control',
                'placeholder' => 'Введите старый пароль',
                'id' => 'old_password'
                ]);
        ?>
  </div>
  <div class="form-group">
        <?
            echo $this->Form->label('new_password', 'Новый пароль');
            echo $this->Form->password('new_password', [
                'class' => 'form-control',
                'placeholder' => 'Введите новый пароль',
                'id' => 'new_password'
                ]);
        ?>
  </div>
  <div class="form-group">
        <?
            echo $this->Form->label('password_confirm', 'Повторите новый пароль');
            echo $this->Form->password('password_confirm', [
                'class' => 'form-control',
                'placeholder' => 'Повторите новый пароль',
                'id' => 'password_confirm'
                ]);
        ?>
  </div>
  <?= $this->Form->button('Сохранить', ['class' => 'btn btn-pastel-red']) ?>
  <?= $this->Form->button('Отмена', ['class' => 'btn btn-pastel-red']) ?>
<?= $this->Form->end() ?>
</div>