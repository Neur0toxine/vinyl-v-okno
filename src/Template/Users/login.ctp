<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;

$this->layout = false;
$pageTitle = 'Винил в окно';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div style="max-width:27rem;margin:0 auto;">
  <?= $this->Form->create() ?>
  <div class="form-group">
  	<?= $this->Form->label('email', 'E-Mail'); ?>
    <?= $this->Form->text('email', [
    		'type' => 'email',
	    	'class' => 'form-control',
	    	'id' => 'email',
	    	'aria-describedby' => 'emailHelp',
	    	'placeholder' => 'Введите E-Mail'
    	]) ?>
    <small id="emailHelp" class="form-text text-muted">Пожалуйста, не используйте ник при входе</small>
  </div>
  <div class="form-group">
    <?= $this->Form->label('password', 'Пароль'); ?>
    <?= $this->Form->text('password', [
    		'type' => 'password',
    		'class' => 'form-control',
    		'id' => 'password',
    		'placeholder' => 'Введите пароль'
    	]) ?>
  </div>
  <?= $this->Form->button('Вход', ['class' => 'btn btn-pastel-red']) ?>
<?= $this->Form->end() ?>
</div>