<?php
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Core\Plugin;
use Cake\Datasource\ConnectionManager;
use Cake\Error\Debugger;
use Cake\Network\Exception\NotFoundException;
use App\Controller\ProductsController;

$this->layout = false;

$pageTitle = 'Винил в окно';
?>

<? $this->extend('/Layout/default') ?>
<?= $this->element('MainMenu'); ?>
<div style="max-width:27rem;margin:0 auto;">
<?= $this->Form->create($user) ?>
  <p class="lead">Ваши данные</p>
  <div class="form-group">
        <?
            echo $this->Form->label('firstname', 'Ваше имя');
            echo $this->Form->text('firstname', [
                'class' => 'form-control',
                'placeholder' => 'Введите ваше имя',
                'id' => 'firstname'
                ]);
        ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('lastname', 'Ваша фамилия'); ?>
    <?= $this->Form->text('lastname', [
        'type' => 'text',
        'class' => 'form-control',
        'id' => 'lastname',
        'placeholder' => 'Введите вашу фамилию'
      ]) ?>
  </div>
  <div class="form-group">
    <?= $this->Form->label('middlename', 'Ваше отчество'); ?>
    <?= $this->Form->text('middlename', [
        'type' => 'text',
        'class' => 'form-control',
        'id' => 'middlename',
        'placeholder' => 'Введите ваше отчество'
      ]) ?>
  </div>
  <div class="form-group">
    <label for="address">Ваш адрес</label>
    <?= $this->Form->textarea('address', ['class' => 'form-control', 'id' => 'address', 'rows' => '2', 'placeholder' => 'Введите сюда адрес']); ?>
  </div>
  <div class="form-group">
    <label for="born">Дата рождения</label>
    <?= $this->Form->text('born', ['class' => 'form-control', 'id' => 'born']); ?>
  </div>
  <div class="d-flex flex-row flex-wrap justify-content-center">
  <?= $this->Form->button('Сохранить', ['class' => 'btn btn-pastel-red mx-1 mb-1']) ?>
  <a href="/users/editpassword" class="btn btn-pastel-red mx-1 mb-1">Сменить пароль</a>
  <?= $this->Form->button('Отмена', ['class' => 'btn btn-pastel-red mx-1 mb-1']) ?>
  </div>
<?= $this->Form->end() ?>
</div>