<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Feedback $feedback
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $feedback->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $feedback->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Feedback'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="feedback form large-9 medium-8 columns content">
    <?= $this->Form->create($feedback) ?>
    <fieldset>
        <legend><?= __('Edit Feedback') ?></legend>
        <?php
            echo $this->Form->control('email');
            echo $this->Form->control('contents');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
