<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Feedback[]|\Cake\Collection\CollectionInterface $feedback
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12">
<div class="products index large-9 medium-8 columns content">
    <h3><?= __('Отзывы') ?></h3>
    <table class="table table-bordered table-striped table-sm table-hover table-responsive-lg">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id', '#') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email', 'E-Mail пользователя') ?></th>
                <th scope="col" class="actions"><?= __('Действия') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($feedback as $feedback): ?>
            <tr>
                <td><?= $this->Number->format($feedback->id) ?></td>
                <td><?= h($feedback->email) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Просмотр'), ['action' => 'view', $feedback->id]) ?>
                    <?= $this->Form->postLink(__('Удалить'), ['action' => 'delete', $feedback->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $feedback->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('В начало')) ?>
            <?= $this->Paginator->prev('< ' . __('Назад')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Вперёд') . ' >') ?>
            <?= $this->Paginator->last(__('В конец') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Страница {{page}} из {{pages}}, показано {{current}} записей из {{count}} всего')]) ?></p>
    </div>
</div>
</div>
</div>