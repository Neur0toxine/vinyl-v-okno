<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Feedback $feedback
 */
 $this->layout = false;
 $this->extend('/Layout/admin');
?>
<div class="row mt-2">
    <div class="col-12 d-flex flex-column justify-content-center">
        <div class="btn-group btn-sm" role="group" aria-label="Действия">
        <?= $this->Form->postLink(__('Удалить отзыв'), ['action' => 'delete', $feedback->id], ['confirm' => __('Вы действительно хотите удалить этот элемент (ID: #{0})?', $feedback->id), 'class' => 'btn btn-secondary']) ?> 
        <?= $this->Html->link(__('Список отзывов'), ['action' => 'index'], ['class' => 'btn btn-secondary']) ?> 
        </div>
    </div>
    <div class="col-12">
        <h3><?= 'ID отзыва: ' . h($feedback->id) ?></h3>
        <table class="table table-bordered table-striped table-sm table-hover">
            <tr>
                <th scope="row"><?= __('E-Mail') ?></th>
                <td><?= h($feedback->email) ?></td>
            </tr>
        </table>
        <div class="col-12">
            <h4><?= __('Содержимое') ?></h4>
            <?= $this->Text->autoParagraph(h($feedback->contents)); ?>
        </div>
    </div>
</div>