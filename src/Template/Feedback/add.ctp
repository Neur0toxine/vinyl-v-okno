<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Feedback $feedback
 */
 $this->layout = false;
 $this->extend('/Layout/default');
 $preTitle = 'Отзыв'
?>
<?= $this->element('MainMenu'); ?>
<div class="row p-4">
    <div class="col-xs-12 col-sm-10 col-md-8 col-lg-6 col-xl-6 p-0">
        <?= $this->Form->create($feedback) ?>
          <div class="form-group">
            <label for="InputEmail">Адрес E-Mail</label>
            <?= $this->Form->email('email', ['class' => 'form-control', 'id' => 'inputEmail', 'aria-describedby' => 'emailHelp', 'placeholder' => 'Введите E-Mail']); ?>
            <small id="emailHelp" class="form-text text-muted">Отправляя эту форму вы соглашаетесь с нашей политикой конфиденциальности</small>
          </div>
          <div class="form-group">
            <label for="text">Текст обращения</label>
            <?= $this->Form->textarea('contents', ['class' => 'form-control', 'id' => 'text', 'rows' => '3', 'placeholder' => 'Введите текст отзыва']); ?>
          </div>
          <?= $this->Form->button(__('Отправить'), ['class' => 'btn btn-pastel-red']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>