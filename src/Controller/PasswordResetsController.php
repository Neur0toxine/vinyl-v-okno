<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use App\Controller\UsersController;

/**
 * PasswordResets Controller
 *
 * @property \App\Model\Table\PasswordResetsTable $PasswordResets
 *
 * @method \App\Model\Entity\PasswordReset[] paginate($object = null, array $settings = [])
 */
class PasswordResetsController extends AppController
{

    public function claim()
    {
        $this->set('title', 'Сброс пароля - '  . Configure::read('title'));
        $users = new UsersController();
        if($users->isLoggedIn())
            return $this->redirect($this->referer());
        if($this->request->is('post'))
        {
            $token = $this->request->query['token'];
            $resets = TableRegistry::get('PasswordResets');
            $reset = $resets->find('all')->where(['token' => $token])->first();
            if($reset)
            {
                $user = $users->Users->get($reset->user_id);
                if($user)
                {
                    $formData = $this->request->getData();
                    $user->password = $formData['password'];
                    if($users->Users->save($user))
                    {
                        $resets->delete($reset);
                        $this->Flash->success(__('Пароль изменён'));
                        return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
                    }
                    else
                        $this->Flash->error(__('Не удалось изменить пароль'));
                }
                else
                    $this->Flash->error(__('Пользователь не найден'));
            }
            else
                $this->Flash->error(__('Не найден запрос восстановления'));
        }
    }

/*
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $passwordResets = $this->paginate($this->PasswordResets);

        $this->set(compact('passwordResets'));
        $this->set('_serialize', ['passwordResets']);
    }

    public function view($id = null)
    {
        $passwordReset = $this->PasswordResets->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('passwordReset', $passwordReset);
        $this->set('_serialize', ['passwordReset']);
    }

    public function add()
    {
        $passwordReset = $this->PasswordResets->newEntity();
        if ($this->request->is('post')) {
            $passwordReset = $this->PasswordResets->patchEntity($passwordReset, $this->request->getData());
            if ($this->PasswordResets->save($passwordReset)) {
                $this->Flash->success(__('The password reset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The password reset could not be saved. Please, try again.'));
        }
        $users = $this->PasswordResets->Users->find('list', ['limit' => 200]);
        $this->set(compact('passwordReset', 'users'));
        $this->set('_serialize', ['passwordReset']);
    }

    public function edit($id = null)
    {
        $passwordReset = $this->PasswordResets->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $passwordReset = $this->PasswordResets->patchEntity($passwordReset, $this->request->getData());
            if ($this->PasswordResets->save($passwordReset)) {
                $this->Flash->success(__('The password reset has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The password reset could not be saved. Please, try again.'));
        }
        $users = $this->PasswordResets->Users->find('list', ['limit' => 200]);
        $this->set(compact('passwordReset', 'users'));
        $this->set('_serialize', ['passwordReset']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $passwordReset = $this->PasswordResets->get($id);
        if ($this->PasswordResets->delete($passwordReset)) {
            $this->Flash->success(__('The password reset has been deleted.'));
        } else {
            $this->Flash->error(__('The password reset could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    */
}
