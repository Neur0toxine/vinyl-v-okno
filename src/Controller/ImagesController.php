<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\ForbiddenException;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;
use App\Controller\UsersController;

class ImagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Network\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */

    public function upload()
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        if($this->request->is("post"))
        {
            $data = $this->request->getData();
            file_put_contents(WWW_ROOT . '/img/products/' . $data['filename'], $data['data']);
        }
    }
}
