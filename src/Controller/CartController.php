<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\UsersController;
use App\Controller\ProductsController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;

/**
 * Cart Controller
 *
 * @property \App\Model\Table\CartTable $Cart
 *
 * @method \App\Model\Entity\Cart[] paginate($object = null, array $settings = [])
 */
class CartController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $this->paginate = [
            'contain' => ['Users']
        ];
        $cart = $this->paginate($this->Cart);

        $this->set(compact('cart'));
        $this->set('_serialize', ['cart']);
        $this->set('title', 'Корзины - '  . Configure::read('title'));
    }

    /**
     * View method
     *
     * @param string|null $id Cart id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $cart = $this->Cart->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('cart', $cart);
        $this->set('_serialize', ['cart']);
        $this->set('title', 'Корзина №' . h($cart->id) . ' - '  . Configure::read('title'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $cart = $this->Cart->newEntity();
        if ($this->request->is('post')) {
            $cart = $this->Cart->patchEntity($cart, $this->request->getData());
            if ($this->Cart->save($cart)) {
                $this->Flash->success(__('The cart has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The cart could not be saved. Please, try again.'));
        }
        else 
            $this->set('title', 'Добавление корзины - '  . Configure::read('title'));
        $users = $this->Cart->Users->find('list', ['limit' => 200]);
        $this->set(compact('cart', 'users'));
        $this->set('_serialize', ['cart']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cart id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $cart = $this->Cart->get($id, [
            'contain' => []
        ]);
        if(gettype($cart->contents) == "string")
            $cart->contents = json_decode($cart->contents);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reqData = $this->request->getData();
            $cart = $this->Cart->patchEntity($cart, $reqData);
            $cart->contents = json_decode($reqData['contents']);
            if ($this->Cart->save($cart)) {
                $this->Flash->success(__('Корзина сохранена'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось сохранить корзину'));
        }
        else
            $this->set('title', 'Редактирование корзины №' . $cart->id . ' - '  . Configure::read('title'));
        $users = $this->Cart->Users->find('list', ['limit' => 200]);
        $cart->contents = json_encode($cart->contents);
        $this->set(compact('cart', 'users'));
        $this->set('_serialize', ['cart']);
    }

    public function addproduct()
    {
        if($this->request->is('post'))
        {
            $users = new UsersController();

            if(!$users->isLoggedIn())
            {
                $this->set('message', 'Корзина доступна только зарегистированным пользователям');
                return null;
            }

            $products = new ProductsController();
            $product_id = $this->request->query('product');
            $count = $this->request->query('count');
            $product = $products->Products->get($product_id);

            if(!isset($product_id) || !isset($count) || !$product) 
            {
                $this->set('message', 'Неверные параметры или пластинка не найдена');
                return null;
            }

            $cart = TableRegistry::get('Cart')->find('all')->where(['user_id' => $users->getUserId()])->first();
            if(!$cart)
            {
                $cart = $this->Cart->newEntity();
                $cart = $this->Cart->patchEntity($cart, ['user_id' => $users->getUserId(), 'contents' => '[]']);
            }

            $cart_contents = $cart->contents;
            if(gettype($cart_contents) == "string")
                $cart_contents = json_decode($cart_contents);

            $elem = json_decode('{"id":"' . $product_id . '","count":"' . $count . '"}');

            $fullprice = 0;
            foreach($cart_contents as $k => $e) 
            {
                if($e['id'] == $product_id)
                {
                    $e['count'] = $e['count'] + $count;
                    $cart_contents[$k] = $e;
                    $elem = null;
                }
                $p = TableRegistry::get('Products')
                        ->find('all')
                        ->select(['price'])
                        ->where(['id' => $e['id']])
                        ->first();
                if($p)
                    $fullprice += $p->price * $e['count'];
            }

            if($elem)
            {
                array_push($cart_contents, $elem);
                $p = TableRegistry::get('Products')
                        ->find('all')
                        ->select(['price'])
                        ->where(['id' => $product_id])
                        ->first();
                $fullprice += $p->price * $count;
            }

            $cart = $this->Cart->patchEntity($cart, ['user_id' => $users->getUserId(), 'contents' => $cart_contents]);

            if($this->Cart->save($cart))
            {
                $this->set('message', 'Пластинка добавлена в корзину');
                $this->set('success', true);
                $this->set('cost', $fullprice);
            }
            else
                $this->set('message', 'Ошибка добавления пластинки в корзину');
        }
    }

    public function removeproduct()
    {
        if($this->request->is('post'))
        {
            $users = new UsersController();

            if(!$users->isLoggedIn())
            {
                $this->set('message', 'Корзина доступна только зарегистированным пользователям');
                return null;
            }

            $product_id = $this->request->query('product');
            if(!isset($product_id)) 
                $product_id = 'all';
            $count = $this->request->query('count');
            if(!isset($count)) 
                $count = 'all';

            $cart = TableRegistry::get('Cart')->find('all')->where(['user_id' => $users->getUserId()])->first();
            if(!$cart)
            {
                $this->set('message', 'Корзина пуста');
                return null;
            }

            if($product_id == 'all')
            {
                $cart->contents = [];
                if($this->Cart->save($cart))
                    $this->set('message', 'Корзина очищена');
                else
                    $this->set('message', 'Не удалось очистить корзину');
                $this->set('success', true);
                return null;
            }

            $cart_contents = $cart->contents;
            if(gettype($cart_contents) == "string")
                $cart_contents = json_decode($cart_contents);

            if(count($cart_contents) == 0)
            {
                $this->set('message', 'Корзина пуста');
                return null;
            }

            foreach($cart_contents as $k => $e) 
            {
                if($e['id'] == $product_id)
                {
                    if($count == 'all' || $count >= $e['count'])
                        unset($cart_contents[$k]);
                    else
                    {
                        $e['count'] = $e['count'] - $count;
                        $cart_contents[$k] = $e;
                        continue;
                    }
                }
            }

            $fullprice = 0;
            foreach($cart_contents as $e)
            {
                if(!$e) continue;
                $p = TableRegistry::get('Products')
                        ->find('all')
                        ->select(['price'])
                        ->where(['id' => $e['id']])
                        ->first();
                if($p)
                    $fullprice += $p->price * $e['count'];
            }

            if(count($cart_contents) == 0)
                $cart_contents = '[]';
            else
                $cart_contents = array_values($cart_contents);

            $cart = $this->Cart->patchEntity($cart, ['user_id' => $users->getUserId(), 'contents' => $cart_contents]);

            if($this->Cart->save($cart))
            {
                $this->set('message', 'Пластинка удалена из корзины');
                $this->set('success', true);
                $this->set('cost', $fullprice);
            }
            else
                $this->set('message', 'Ошибка удаления пластинки из корзины');
        }
    }

    public function show()
    {
        $users = new UsersController();
        $cart = TableRegistry::get('Cart')->find()->where(['user_id' => $users->getUserId()])->first();
        if(!$cart)
        {
            $this->Flash->success(__('Корзина пуста'));
            $this->set('products', []);
            return null;
        }

        $cart_items = array();
        $cart_contents = $cart->contents;

        if(gettype($cart_contents) == "string")
            $cart_contents = json_decode($cart_contents);

        $products = new ProductsController();
        foreach($cart_contents as $e)
        {
            $product = $products->Products->get($e['id']);
            if($product)
                array_push($cart_items, ['product' => $product, 'count' => $e['count']]);
        }
        $this->set('products', $cart_items);
        $this->set('title', 'Корзина - '  . Configure::read('title'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Cart id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        
        $this->request->allowMethod(['post', 'delete']);
        $cart = $this->Cart->get($id);
        if ($this->Cart->delete($cart)) {
            $this->Flash->success(__('Корзина удалена'));
        } else {
            $this->Flash->error(__('Не удалось удалить корзину'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function beforeFilter(Event $event)
    {
        $unlocked = array('addproduct', 'removeproduct');
        $this->Security->setConfig('unlockedActions', $unlocked);
        if(in_array($this->request->getParam('action'), $unlocked)) 
            $this->eventManager()->off($this->Csrf);
    }
}
