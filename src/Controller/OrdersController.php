<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Core\Configure;
/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[] paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $this->paginate = [
            'contain' => ['Users']
        ];
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
        $this->set('title', 'Заказы - '  . Configure::read('title'));
    }

    public function show()
    {
        $orders = $this->paginate(
            TableRegistry::get('Orders')
                ->find('all')
                ->where(['user_id' => $this->Auth->user('id')])
                ->order(['timestamp' => 'DESC']));

        $this->set(compact('orders'));
        $this->set('_serialize', ['orders']);
        $this->set('title', 'Заказы - '  . Configure::read('title'));
    }

    public function create()
    {
        $carts = TableRegistry::get('Cart');
        $products = TableRegistry::get('Products');
        $cart = $carts->find('all')->where(['user_id' => $this->Auth->user('id')])->first();
        $contents = ['data' => [], 'costs' => 0];
        foreach($cart->contents as $c)
        {
            $product = $products
                ->find('all')
                ->select(['price'])
                ->where(['id' => $c['id']])
                ->first();

            if($product)
            {
                array_push($contents['data'], 
                    [
                        'id' => $c['id'],
                        'price' => $product->price,
                        'count' => $c['count']
                    ]);
                $contents['costs'] = $contents['costs'] + ($product->price * $c['count']);
            }
        }
        $order = $this->Orders->newEntity();
        $order->user_id = $this->Auth->user('id');
        $order->track = '';
        $order->state = 1;
        $order->contents = $contents;
        if($this->Orders->save($order))
        {
            $cart->contents = [];
            $carts->save($cart);
            $this->Flash->success(__('Заказ оформлен'));
            $this->redirect(['controller' => 'Orders', 'action' => 'show']);
        }
        else
        {
            $this->Flash->error(__('Не удалось оформить заказ'));
            $this->redirect(['controller' => 'Cart', 'action' => 'show']);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $order = $this->Orders->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('order', $order);
        $this->set('_serialize', ['order']);
        $this->set('title', 'Заказ №' . $order->id . ' - '  . Configure::read('title'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('Заказ добавлен.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $this->set(compact('order', 'users'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reqData = $this->request->getData();
            $order = $this->Orders->patchEntity($order, $reqData);
            $order->contents = json_decode($reqData['contents']);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('Заказ сохранён'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось сохранить заказ!'));
        }
        else
            $this->set('title', 'Редактирование заказа №' . $order->id . ' - '  . Configure::read('title'));
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $order->contents = json_encode($order->contents);
        $this->set(compact('order', 'users'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('Заказ удалён'));
        } else {
            $this->Flash->error(__('Не удалось удалить заказ'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
