<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Event\Event;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[] paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout']);
        $this->Auth->allow(['logout', 'add']);
    }

    public function beforeFilter(Event $event)
    {
        $actions = [
            'register',
        ];

        if (in_array($this->request->params['action'], $actions)) {
            // for csrf
            //$this->eventManager()->off($this->Csrf);

            // for security component
            $this->Security->config('unlockedActions', $actions);
        }
    }

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $tableuser = TableRegistry::get('Users')
                    ->find()
                    ->where(['username' => $user['username']])
                    ->first();
                if($tableuser->active == 1)
                {
                    $this->Auth->setUser($user);
                    return $this->redirect($this->Auth->redirectUrl());
                }
                else 
                {
                    $this->Flash->error('Пожалуйста, активируйте свой аккаунт.');
                    return $this->redirect($this->referer());
                }
            } else {
                $this->Flash->error(__('Неверные учётные данные, или аккаунт ещё не активирован.'));
            }
        }
        else
            $this->set('title', 'Вход - '  . Configure::read('title'));
    }

    public function register()
    {
        if ($this->request->is('post')) {
            $reqData = $this->request->getData();
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $reqData);
            $user->active = 0;
            $user->role = 0;
            $user->born = date_create_from_format('m/d/Y', $reqData['born']);
            if ($this->Users->save($user)) {
                $token = hash('gost', $user->email, false);
                $confirms = TableRegistry::get('UsersConfirmations');
                $confirm = $confirms->newEntity();
                $confirm->user_id = $user->id;
                $confirm->token = $token;
                if($confirms->save($confirm)) {
                    $email = new Email();
                    $email->viewVars(['domain' => Configure::read('domain'),'token' => $token, 'username' => $user->username]);
                    $email
                        ->template('activate')
                        ->emailFormat('html')
                        ->to($user->email)
                        ->subject('Регистрация на сайте ' . Configure::read('domain'))
                        ->from(Configure::read('adminmail'), 'Регистрация на сайте ' . Configure::read('domain'))
                        ->send();
                    $this->Flash->success(__('Регистрация успешна. Проверьте свою электронную почту.'));
                    return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
                }
                else 
                    $this->Flash->error(__('Регистрация не удалась по техническим причинам.'));
            }
            $this->Flash->error(__('Регистрация не удалась! Возможно, вы ошиблись при заполнении.'));
        }
        else
            $this->set('title', 'Регистрация - ' . Configure::read('title'));
    }

    public function resetpassword()
    {
        if($this->Auth->user('id') != 0)
            return $this->redirect($this->referer());
        if($this->request->is('post'))
        {
            $reqData = $this->request->getData();
            $user = TableRegistry::get('Users')->find('all')->where(['email' => $reqData['email']])->first();
            if($user)
            {
                $token = hash('gost', $user->email . $user->username, false);
                $prestable = TableRegistry::get('PasswordResets');
                $res = $prestable->find('all')->where(['user_id' => $user->id])->first();
                if(!$res) $res = $prestable->newEntity();
                $res->user_id = $user->id;
                $res->token = $token;
                if($prestable->save($res))
                {
                    $email = new Email();
                    $email->viewVars(['domain' => Configure::read('domain'), 'token' => $token, 'username' => $user->username]);
                    $email
                        ->template('passwordreset')
                        ->emailFormat('html')
                        ->to($user->email)
                        ->subject('Восстановление пароля ' . Configure::read('domain'))
                        ->from(Configure::read('adminmail'), 'Восстановление пароля ' . Configure::read('domain'))
                        ->send();
                    $this->Flash->success(__('Пожалуйста, проверьте свой E-Mail'));
                }
                else
                    $this->Flash->error(__('Похоже, Вы уже запрашивали восстановление пароля'));
            }
            else
                $this->Flash->error(__('Пользователь не найден'));
        }
        else
            $this->set('title', 'Сброс пароля - '  . Configure::read('title'));
    }

    public function logout()
    {
        $this->Flash->success('Вы разлогинены');
        return $this->redirect($this->Auth->logout());
    }

    public function getUserId()
    {
        return $this->Auth->user('id');
    }

    public function isLoggedIn()
    {
        return $this->Auth->user('id') != 0;
    }


    public function getUserRole($userid)
    {
        if($userid == null) return 0;
        else
        {
            $table = TableRegistry::get('Users');
            return $table
                    ->find()
                    ->select('role')
                    ->where(['id' => $userid])
                    ->first()->role;
        }
    }

    public function isAdmin($userid = null)
    {
        $uid = $this->getUserId();
        if($userid != null) $uid = $userid;
        return $this->getUserRole($uid) == 1;
    }

    public function getUserName($userid)
    {
        if($userid == null) return '';
        else
        {
            $table = TableRegistry::get('Users');
            return $table
                    ->find()
                    ->select('username')
                    ->where(['id' => $userid])
                    ->first()->username;
        }
    }

    public function profile($id = null)
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reqData = $this->request->getData();
            $user->firstname = $reqData['firstname'];
            $user->lastname = $reqData['lastname'];
            $user->middlename = $reqData['middlename'];
            $user->address = $reqData['address'];
            $user->born = $reqData['born'];
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Данные успешно обновлены'));
            }
            else
                $this->Flash->error(__('Ошибка обновления данных'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
        $this->set('title', 'Профиль - '  . Configure::read('title'));
    }

    public function editpassword($id = null)
    {
        $user = $this->Users->get($this->Auth->user('id'), [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $reqData = $this->request->getData();
            $old_password = $reqData['old_password'];

            if((new DefaultPasswordHasher)->check($old_password, $user->password) == false)
            {
                $this->Flash->error(__('Неверный старый пароль'));
                return null;
            }

            $new_password = $reqData['new_password'];
            if($new_password != $reqData['password_confirm'])
            {
                $this->Flash->error(__('Пароли не совпадают'));
                return null;
            }

            if($new_password == "")
            {
                $this->Flash->error(__('Пароль не может быть пустым'));
                return null;
            }

            $user->password = $new_password;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Пароль изменён'));
                $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }
            else
                $this->Flash->error(__('Ошибка обновления данных'));
        }
        else
            $this->set('title', 'Смена пароля - '  . Configure::read('title'));
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if(!$this->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $users = $this->paginate($this->Users);
        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
        $this->set('title', 'Пользователи - '  . Configure::read('title'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if(!$this->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $user = $this->Users->get($id, [
            'contain' => ['Cart', 'Orders']
        ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
        $this->set('title', h($user->username) . ' - '  . Configure::read('title'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if(!$this->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Пользователь сохранён'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось сохранить пользователя'));
        }
        else
            $this->set('title', 'Добавление пользователя - '  . Configure::read('title'));
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if(!$this->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Пользователь сохранён'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось сохранить пользователя'));
        }
        else
            $this->set('title', 'Изменение пользователя ' . h($user->username) . ' - '  . Configure::read('title'));
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if(!$this->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
