<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use App\Controller\UsersController;
use App\Controller\CategoriesController;
use Cake\Core\Configure;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[] paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        
        $products = $this->paginate($this->Products);
        $this->set(compact('products'));
        $this->set('_serialize', ['products']);
        $this->set('title', 'Пластинки - '  . Configure::read('title'));
    }

    public function display($options = [], $conditions = [], $order = [], $page = 1, $productsPerPage = 40) 
    {
        $products = TableRegistry::get('Products');
        $query = $products
            ->find()
            ->select($options)
            ->where($conditions)
            ->offset(($page-1) * $productsPerPage)
            ->limit($productsPerPage)
            ->order($order);
        return $query;
    }

    public function navigator()
    {
        $page = $this->request->query('page');
        if(!isset($page))
            $page = 1;
        if($page <= 1)
            $page = 1;
        $productsPerPage = 40;

        $categories = TableRegistry::get('Categories');
        $categories_list = $categories->find('all')->select(['id', 'name']);
        $this->set('categories', $categories_list);

        $sel = [];
        $name = $this->request->query('name');
        $yfrom = $this->request->query('yfrom');
        $yto = $this->request->query('yto');
        $ratingmin = $this->request->query('ratingmin');
        $ratingmax = $this->request->query('ratingmax');
        $ratesmin = $this->request->query('ratesmin');
        $boughtmin = $this->request->query('boughtmin');
        $pricemin = $this->request->query('pricemin');
        $pricemax = $this->request->query('pricemax');
        $categories = json_decode($this->request->query('genres'));

        if(isset($name)) $sel['Products.name LIKE'] = '%' . $name . '%';
        if(isset($yfrom)) $sel['year >='] = $yfrom;
        if(isset($yto)) $sel['year <='] = $yto;
        if(isset($ratingmin)) $sel['rating >='] = $ratingmin;
        if(isset($ratingmax)) $sel['rating <='] = $ratingmax;
        if(isset($ratesmin)) $sel['rating_count >='] = $ratesmin;
        if(isset($boughtmin)) $sel['bought >='] = $boughtmin;
        if(isset($pricemin)) $sel['price >='] = $pricemin;
        if(isset($pricemax)) $sel['price <='] = $pricemax;
        if(!isset($categories)) $categories = [];
        //$sel["tracks REGEXP"] = '(.*)?у(.*)?';
        /*
        if(isset($authors)) if(count($authors) > 0)
        {
            $sel["authors REGEXP"] = '(.*)?';
            foreach($authors as $e)
                $sel["authors REGEXP"] = $sel["authors REGEXP"] . $e;
            $sel["authors REGEXP"] = $sel["authors REGEXP"] . '(.*)?';
            $sel["authors REGEXP"] = "'" . $sel["authors REGEXP"] . "'";
        }
        */

        $connection = ConnectionManager::get('default');
        $connection->execute("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
        $products = TableRegistry::get('Products');
        $query = $products
            ->find()
            ->matching('Categories', function ($q) use( &$categories) {
                if(count($categories))
                    return $q->where(['Categories.id IN' => $categories]);
                else 
                    return $q;
            })
            ->select()
            ->where($sel)
            ->offset(($page-1) * $productsPerPage)
            ->limit($productsPerPage)
            ->order(['Products.id' => 'DESC'])
            ->group(['Products.id'])
            ->map(function($e){
                $d = false;
                $e_authors = $e->authors;
                $e_tracks = $e->tracks;
                if(gettype($e->authors) == "string")
                    $e_authors = json_decode($e->authors);
                if(gettype($e->tracks) == "string")
                    $e_tracks = json_decode($e->tracks);
                $to_find = implode(',', $e_authors);
                $to_find_t = implode(',', $e_tracks);
                $authors = json_decode($this->request->query('authors'));
                $tracks = json_decode($this->request->query('tracks'));
                $a_found = false;
                $t_found = false;
                if($authors)
                {
                    foreach($authors as $a)
                        if(stristr($to_find, $a) !== false)
                        {
                            $d = true;
                            $a_found = true;
                        }
                }
                if($tracks)
                {
                    foreach($tracks as $a)
                        if(stristr($to_find_t, $a) !== false)
                        {
                            $d = true;
                            $t_found = true;
                        }
                }
                if($authors && $tracks)
                    $d = ($a_found && $t_found);
                if(!$authors && !$tracks)
                    $d = true;
                if($d) return $e;
            });

        $this->set('products', $query);
        $this->set('title', 'Поиск - '  . Configure::read('title'));
    }

    public function rate()
    {
        if($this->request->is('post'))
        {
            $users = new UsersController();
            if(!$users->isLoggedIn())
            {
                $this->set('message', 'Оценки могут ставить только зарегистрированные пользователи');
                $this->set('rate', 0);
                return null;
            }
            else
            {
                $product_id = $this->request->query['product'];
                $mark = $this->request->query['mark'];
                $product = $this->Products->get($product_id);
                if(!isset($mark) || !isset($product_id) || ($mark <= 0) || ($mark > 5) || !$product)
                {
                    $this->set('message', 'Неверные параметры запроса, или пластинку только что удалили.');
                    $this->set('rate', 0);
                    return null;
                }
                $coeff = 0;
                if($product->rating_count == 0) 
                    $coeff = $mark;
                else
                    $coeff += (($mark - $product->rating) / ($product->rating_count + 1));
                $product->rating += $coeff;
                $product->rating_count++;
                if($this->Products->save($product))
                {
                    $this->set('message', 'Спасибо, Ваша оценка учтена!');
                    $this->set('rate', $coeff);
                }
            }
        }
        else
        {
            $this->set('message', 'Этот маршрут принимает только POST-запросы.');
            $this->set('rate', 0);
        }
    }


    public function show($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Categories']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
        $this->set('title', h($product->name) . ' - '  . Configure::read('title'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $product = $this->Products->get($id, [
            'contain' => ['Categories']
        ]);

        $this->set('product', $product);
        $this->set('_serialize', ['product']);
        $this->set('title', h($product->name) . ' - '  . Configure::read('title'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $reqData = $this->request->getData();
            $product = $this->Products->patchEntity($product, $reqData);

            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 20; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $imgBigName = '/img/products/' . $randomString . '_300x300.jpg';
            $imgSmallName = '/img/products/' . $randomString . '_152x152.jpg';

            $imagine = new \Imagine\Imagick\Imagine();
            $image_big = $imagine->open($reqData['photo']['tmp_name']);
            $image_big->resize(new \Imagine\Image\Box(300, 300))->save(WWW_ROOT . $imgBigName, array('jpeg_quality' => 80));
            $image_big->resize(new \Imagine\Image\Box(152, 152))->save(WWW_ROOT . $imgSmallName, array('jpeg_quality' => 80));
            
            // For pure imagick module
            // $image = new \Imagick($reqData['photo']['tmp_name']);
            // //$image->readImageBlob($reqData['photo']['tmp_name']);
            // $image->setImageFormat('jpg');
            // $image->setImageCompression(\Imagick::COMPRESSION_JPEG);
            // $image->setImageCompressionQuality(80);
            // $image->thumbnailImage(300, 300);
            // $image->writeImage(WWW_ROOT . $imgBigName);
            // $image->thumbnailImage(152, 152);
            // $image->writeImage(WWW_ROOT . $imgSmallName);


            $product->img_big = $imgBigName;
            $product->img_small = $imgSmallName;

            $product->authors = json_decode($reqData['authors']);
            $product->tracks = json_decode($reqData['tracks']);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Пластинка успешно добавлена.'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось добавить пластинку. Может, вы ошиблись при вводе?.'));
        }
        $categories = $this->Products->Categories->find('list', ['limit' => 200]);
        $this->set(compact('product', 'categories'));
        $this->set('_serialize', ['product']);
        $this->set('title', 'Добавление пластинки - '  . Configure::read('title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $product = $this->Products->get($id, [
            'contain' => ['Categories']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $reqData = $this->request->getData();
            $product = $this->Products->patchEntity($product, $reqData);

            if(isset($reqData['photo']))
            {
                $imagine = new \Imagine\Imagick\Imagine();
                $image_big = $imagine->open($reqData['photo']['tmp_name']);
                $image_big->resize(new \Imagine\Image\Box(300, 300))->save(WWW_ROOT . $product->img_big, array('jpeg_quality' => 80));
                $image_big->resize(new \Imagine\Image\Box(152, 152))->save(WWW_ROOT . $product->img_small, array('jpeg_quality' => 80));
            }

            $product->authors = json_decode($reqData['authors']);
            $product->tracks = json_decode($reqData['tracks']);
            if ($this->Products->save($product)) {
                $this->Flash->success(__('Пластинка успешно сохранена.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось сохранить пластинку. Может, вы ошиблись при вводе?'));
        }
        $categories = $this->Products->Categories->find('list', ['limit' => 200]);
        $product->authors = json_encode($product->authors);
        $product->tracks = json_encode($product->tracks);
        $this->set(compact('product', 'categories'));
        $this->set('_serialize', ['product']);
        $this->set('title', 'Редактирование пластинки "' . h($product->name) . '" - '  . Configure::read('title'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        $imgBig = $product->img_big;
        $imgSmall = $product->img_small;
        if ($this->Products->delete($product)) {
            unlink(WWW_ROOT . $imgBig);
            unlink(WWW_ROOT . $imgSmall);
            $this->Flash->success(__('Пластинка удалена.'));
        } else {
            $this->Flash->error(__('Не удалось удалить пластинку.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(Event $event)
    {
        $unlocked = array('rate');
        $this->Security->setConfig('unlockedActions', $unlocked);
        if(in_array($this->request->getParam('action'), $unlocked)) 
            $this->eventManager()->off($this->Csrf);
    }
}
