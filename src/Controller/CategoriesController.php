<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Categories Controller
 *
 * @property \App\Model\Table\CategoriesTable $Categories
 *
 * @method \App\Model\Entity\Category[] paginate($object = null, array $settings = [])
 */
class CategoriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $categories = $this->paginate($this->Categories);

        $this->set(compact('categories'));
        $this->set('_serialize', ['categories']);
        $this->set('title', 'Жанры - '  . Configure::read('title'));
    }

    /**
     * View method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $category = $this->Categories->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('category', $category);
        $this->set('_serialize', ['category']);
        $this->set('title', h($category->name) . ' - '  . Configure::read('title'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $category = $this->Categories->newEntity();
        if ($this->request->is('post')) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('Жанр сохранён'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось сохранить жанр'));
        }
        else
            $this->set('title', 'Добавление жанра - '  . Configure::read('title'));
        $products = $this->Categories->Products->find('list', ['limit' => 200]);
        $this->set(compact('category', 'products'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);

        $category = $this->Categories->get($id, [
            'contain' => ['Products']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $category = $this->Categories->patchEntity($category, $this->request->getData());
            if ($this->Categories->save($category)) {
                $this->Flash->success(__('Жанр обновлён'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Не удалось обновить жанр'));
        }
        else
            $this->set('title', 'Редактирование "' . $category->name . '" - '  . Configure::read('title'));
        $products = $this->Categories->Products->find('list', ['limit' => 200]);
        $this->set(compact('category', 'products'));
        $this->set('_serialize', ['category']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Category id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
         $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        
        $this->request->allowMethod(['post', 'delete']);
        $category = $this->Categories->get($id);
        if ($this->Categories->delete($category)) {
            $this->Flash->success(__('Жанр удалён'));
        } else {
            $this->Flash->error(__('Не удалось обновить жанр'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
