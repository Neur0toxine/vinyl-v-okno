<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;

/**
 * Feedback Controller
 *
 * @property \App\Model\Table\FeedbackTable $Feedback
 *
 * @method \App\Model\Entity\Feedback[] paginate($object = null, array $settings = [])
 */
class FeedbackController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        $feedback = $this->paginate($this->Feedback);

        $this->set(compact('feedback'));
        $this->set('_serialize', ['feedback']);
        $this->set('title', 'Отзывы - '  . Configure::read('title'));
    }

    /**
     * View method
     *
     * @param string|null $id Feedback id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        $feedback = $this->Feedback->get($id, [
            'contain' => []
        ]);

        $this->set('feedback', $feedback);
        $this->set('_serialize', ['feedback']);
        $this->set('title', 'Отзыв №' . $feedback->id . ' - '  . Configure::read('title'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $feedback = $this->Feedback->newEntity();
        if ($this->request->is('post')) {
            $feedback = $this->Feedback->patchEntity($feedback, $this->request->getData());
            if ($this->Feedback->save($feedback)) {
                $this->Flash->success(__('Отзыв успешно добавлен!'));

                return $this->redirect(['controller' => 'Pages', 'action' => 'display', 'home']);
            }
            $this->Flash->error(__('Невозможно добавить отзыв, попробуйте позже.'));
        }
        $this->set(compact('feedback'));
        $this->set('_serialize', ['feedback']);
        $this->set('title', 'Отзыв - '  . Configure::read('title'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Feedback id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        $feedback = $this->Feedback->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $feedback = $this->Feedback->patchEntity($feedback, $this->request->getData());
            if ($this->Feedback->save($feedback)) {
                $this->Flash->success(__('The feedback has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The feedback could not be saved. Please, try again.'));
        }
        $this->set(compact('feedback'));
        $this->set('_serialize', ['feedback']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Feedback id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $users = new UsersController();
        if(!$users->isAdmin())
            return $this->redirect(['controller' => 'Pages', 'action' => 'home']);
        $this->request->allowMethod(['post', 'delete']);
        $feedback = $this->Feedback->get($id);
        if ($this->Feedback->delete($feedback)) {
            $this->Flash->success(__('Отзыв удалён.'));
        } else {
            $this->Flash->error(__('Невозможно удалить отзыв.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
