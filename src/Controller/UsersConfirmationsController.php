<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * UsersConfirmations Controller
 *
 * @property \App\Model\Table\UsersConfirmationsTable $UsersConfirmations
 *
 * @method \App\Model\Entity\UsersConfirmation[] paginate($object = null, array $settings = [])
 */
class UsersConfirmationsController extends AppController
{

    public function activate($token = null)
    {
        $this->set('title', 'Активация аккаунта - '  . Configure::read('title'));
        $c = TableRegistry::get('UsersConfirmations');
        $confirm = $c->find('all')->where(['token' => $token])->first();
        if($confirm) 
        {
            $u = TableRegistry::get('Users');
            $user = $u->get($confirm->user_id);
            $user->active = 1;
            $c->delete($confirm);
            if($u->save($user))
            {
                $this->set('activate_msg', 'Ваш аккаунт успешно активирован.');
                $this->set('prepend_msg', 'Теперь авторизуйтесь используя E-Mail и пароль.');
            }
            else 
            {
                $this->set('activate_msg', 'Ошибка активации аккаунта');
                $this->set('prepend_msg', 'Обратитесь к технической поддержке ресурса.');
            }
        } 
        else
        {
            $this->set('activate_msg', 'Пользователь не найден, или уже активен.');
            $this->set('prepend_msg', 'Попробуйте войти в аккаунт используя E-Mail и пароль.');
        }
    }
    /*
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $usersConfirmations = $this->paginate($this->UsersConfirmations);

        $this->set(compact('usersConfirmations'));
        $this->set('_serialize', ['usersConfirmations']);
    }

    public function view($id = null)
    {
        $usersConfirmation = $this->UsersConfirmations->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('usersConfirmation', $usersConfirmation);
        $this->set('_serialize', ['usersConfirmation']);
    }

    public function add()
    {
        $usersConfirmation = $this->UsersConfirmations->newEntity();
        if ($this->request->is('post')) {
            $usersConfirmation = $this->UsersConfirmations->patchEntity($usersConfirmation, $this->request->getData());
            if ($this->UsersConfirmations->save($usersConfirmation)) {
                $this->Flash->success(__('The users confirmation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users confirmation could not be saved. Please, try again.'));
        }
        $users = $this->UsersConfirmations->Users->find('list', ['limit' => 200]);
        $this->set(compact('usersConfirmation', 'users'));
        $this->set('_serialize', ['usersConfirmation']);
    }

    public function edit($id = null)
    {
        $usersConfirmation = $this->UsersConfirmations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersConfirmation = $this->UsersConfirmations->patchEntity($usersConfirmation, $this->request->getData());
            if ($this->UsersConfirmations->save($usersConfirmation)) {
                $this->Flash->success(__('The users confirmation has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users confirmation could not be saved. Please, try again.'));
        }
        $users = $this->UsersConfirmations->Users->find('list', ['limit' => 200]);
        $this->set(compact('usersConfirmation', 'users'));
        $this->set('_serialize', ['usersConfirmation']);
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersConfirmation = $this->UsersConfirmations->get($id);
        if ($this->UsersConfirmations->delete($usersConfirmation)) {
            $this->Flash->success(__('The users confirmation has been deleted.'));
        } else {
            $this->Flash->error(__('The users confirmation could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    */
}
