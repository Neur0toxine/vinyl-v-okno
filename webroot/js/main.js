var App = {
    genres: [],
    init: function() {
        $('#main-menu-searchbtn').on('click', function(e){
            if(typeof e !== 'undefined')
                e.preventDefault()
            window.location.assign(window.location.origin + 
                '/products/navigator?name=' + encodeURI($('#main-menu-searchinput').val()))
        })
        if(window.location.pathname == '/users/register' || window.location.pathname == '/users/profile') {
            $(document).ready(function(){
                $('#born').datepicker({
                    uiLibrary: 'bootstrap4',
                    iconsLibrary: 'fontawesome'
                })
            })
        }
        if(/\/products\/navigator.*/.test(window.location.pathname)) {
            var params = queryParser.parse(window.location.search)
            $('#vinylName').val(params.name || '')
            $('#year_from').val(params.yfrom || '1970')
            $('#year_to').val(params.yto || new Date().getFullYear())
            $('.searchStarMin > input.star-' + (params.ratingmin || 1)).prop('checked', true)
            $('.searchStarMax > input.star-' + (params.ratingmax || 5)).prop('checked', true)
            $('#minrating').val(params.ratesmin || 1)
            $('#minbought').val(params.boughtmin || 1)
            $('#slmin').val(params.pricemin || 1000)
            $('#slmax').val(params.pricemax || 10000)

            this.genres = (JSON.parse(params.genres || '[]'))
            var authors = (JSON.parse(params.authors || '[]'))
            var tracks = (JSON.parse(params.tracks || '[]'))
            authors.forEach((i) => App.appendBadge('#authorslist',
                        'span', 'badge-my1',
                        i, true, {text:'Кликните для удаления', place:'right'}))
            tracks.forEach((i) => App.appendBadge('#trackslist',
                        'span', 'badge-my1',
                        i, true, {text:'Кликните для удаления', place:'right'}))

            this.slider = $('#cost').slider({tooltip:'hide'})
            $('#minrating').slider({tooltip:'hide'})
            $('#minrating').on('slide', function(e){
                var el = $('#minrating_val')
                el.attr('title', e.value);
                el.tooltip('dispose')
                el.tooltip({animation: false})
                el.tooltip('show')
            })
            $('#minbought').slider({tooltip:'hide'})
            $('#minbought').on('slide', function(e){
                var el = $('#minbought_val')
                el.attr('title', e.value);
                el.tooltip('dispose')
                el.tooltip({animation: false})
                el.tooltip('show')
            })
            $("#cost").on("slide", function(slideEvt) {
                var val = slideEvt.value.toString().split(',')
                $("#slmin").val(val[0]);
                $("#slmax").val(val[1]);
            })
            $('#addauthorbtn').on('click', function(e){
                App.appendBadge('#authorslist',
                        'span', 'badge-my1',
                        $('#authorname').val(), true, {text:'Кликните для удаления', place:'right'})
            })
            $('#addtrackbtn').on('click', function(e){
                App.appendBadge('#trackslist',
                        'span', 'badge-my1',
                        $('#trackname').val(), true, {text:'Кликните для удаления', place:'right'})
            })
            $('#slmin, #slmax').on('change', function(){
                App.slider.slider('setValue', [+$('#slmin').val(), +$('#slmax').val()])
            })
            $("#slmin, #slmax").keydown(function (e) {
                    // Allow: backspace, delete, tab, escape, enter and .
                    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                         // Allow: Ctrl+A, Command+A
                        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
                         // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                             // let it happen, don't do anything
                             return;
                    }
                    // Ensure that it is a number and stop the keypress
                    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                        e.preventDefault();
                    }
            })
            App.slider.slider('setValue', [+$('#slmin').val(), +$('#slmax').val()])
            $('#minrating').slider('setValue', params.ratesmin || 1)
            $('#minbought').slider('setValue', params.boughtmin || 1)
        }
        // Check list
        $(function () {
            $('.list-group.checked-list-box .list-group-item').each(function () {
                
                // Settings
                var $widget = $(this),
                    $checkbox = $('<input type="checkbox" class="hidden" />'),
                    color = ($widget.data('color') ? $widget.data('color') : "primary"),
                    style = ($widget.data('style') == "button" ? "btn-" : "list-group-item-"),
                    settings = {
                        on: {
                            icon: 'fa fa-check-square-o'
                        },
                        off: {
                            icon: 'fa fa-square-o'
                        }
                    };
                    
                $widget.css('cursor', 'pointer')
                $checkbox.attr('value', ($widget.data('value') || ''))
                $widget.append($checkbox);

                // Event Handlers
                $widget.on('click', function () {
                    $checkbox.prop('checked', !$checkbox.is(':checked'));
                    $checkbox.triggerHandler('change');
                    updateDisplay();
                });
                $checkbox.on('change', function () {
                    updateDisplay();
                });
                  

                // Actions
                function updateDisplay() {
                    var isChecked = $checkbox.is(':checked');

                    // Set the button's state
                    $widget.data('state', (isChecked) ? "on" : "off");

                    // Set the button's icon
                    $widget.find('.fa')
                        .removeClass()
                        .addClass('mx-1 fa ' + settings[$widget.data('state')].icon);

                    // Update the button's color
                    if (isChecked) {
                        $widget.addClass('active ' + style + color);
                    } else {
                        $widget.removeClass('active ' + style + color);
                    }
                }

                // Initialization
                function init() {
                    
                    if ($widget.data('checked') == true) {
                        $checkbox.prop('checked', !$checkbox.is(':checked'));
                    }
                    
                    updateDisplay();

                    // Inject the icon if applicable
                    if ($widget.find('.fa').length == 0) {
                        $widget.prepend('<i class="mx-1 fa ' + settings[$widget.data('state')].icon + '" aria-hidden="true"></i>')
                    }
                }
                init();
            });
        });
        setTimeout(function() {
                App.genres.forEach((i) => {
                    $('#genres-list > ul li[data-value="' + i + '"]').click()
            })
        }, 10)
        console.log('app v0.1');
    },
    generateRandomString: function(strlength = 20) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < strlength; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    },
    search: function() {
        var authors = []
        var tracks = []
        var genres = []
        $('#authorslist > span').each((i,e) => {authors.push($(e).text())})
        $('#trackslist > span').each((i,e) => {tracks.push($(e).text())})
        $("#genres-list li.active").each(function(idx, li) {
            genres.push($(li).data('value'))
        })
        var params = {
            name: $('#vinylName').val(),
            yfrom: $('#year_from').val(),
            yto: $('#year_to').val(),
            ratingmin: $('.searchStarMin > input:checked').val(),
            ratingmax: $('.searchStarMax > input:checked').val(),
            ratesmin: $('#minrating').val(),
            boughtmin: $('#minbought').val(),
            pricemin: $('#slmin').val(),
            pricemax: $('#slmax').val(),
            genres: JSON.stringify(genres),
            authors: JSON.stringify(authors),
            tracks: JSON.stringify(tracks)
        }
        window.location.search = queryParser.stringify(params)
    },
    jumpOverPage: function(cnt) {
        var params = queryParser.parse(window.location.search)
        var page = Number.parseInt(params.page || 1)
        page += cnt;
        if(page < 0)
            page = 1
        params.page = page
        console.log(page)
        window.location.search = queryParser.stringify(params)
    },
    appendBadge: function(parentsel, badgetag, styleclass, content, removable, tooltip) {
        var parent = $(parentsel)
        if(!parent) return false
        var badge = document.createElement(badgetag)
        var classList = ['badge', styleclass]
        $(badge).attr('class', classList.join(' '))
        if(tooltip) {
            $(badge).data('toggle', 'tooltip')
            $(badge).data('placement', tooltip.place)
            $(badge).attr('title', tooltip.text)
        }
        $(badge).text(content)
        if(removable) {
            $(badge).on('click', function(e){ $(this).tooltip('hide'),$(this).remove() })
            $(badge).css('cursor', 'pointer')
        }
        parent.append(badge)
        if(tooltip) $(badge).tooltip('enable')
    },
    tooltip: function(text, duration = 2000, closable = true, bgcolor = '', textcolor = '', ttWidth = '27rem') {
        var tooltip = document.createElement('div')
        $(tooltip).attr('class', 'app-tooltip')
        $(tooltip).attr('id', 'tooltip-' + this.generateRandomString())
        $(tooltip).css('margin-top', '2px')
        $(tooltip).css('margin-right', '-' + (ttWidth || '27rem'))
        $(tooltip).css('width', (ttWidth || '27rem'))
        if (bgcolor.length > 0) $(tooltip).css('background', bgcolor)
        if (textcolor.length > 0) $(tooltip).css('color', textcolor)
        $(tooltip).append('<span>' + text + '</span>')

        if (closable) {
            var tooltipClose = document.createElement('div')
            var tooltipIcon = document.createElement('i')
            $(tooltipIcon).attr('class', 'fa fa-times')
            $(tooltipIcon).attr('aria-hidden', 'true')
            $(tooltipIcon).css('border-left', '1px dotted ' + (textcolor || 'white'))
            $(tooltipClose).attr('class', 'app-tooltip-close')
            $(tooltipClose).attr('onclick', 'App.closeTooltip(\'' + tooltip.id + '\')')
            $(tooltipClose).append(tooltipIcon)
            $(tooltip).append(tooltipClose)
        }

        $('#tooltips-container').prepend(tooltip);

        $(tooltip).animate({
            marginRight: '0rem',
            opacity: '+=1'
        }, {
            duration: 300,
        });

        if (duration > 0) setTimeout(() => {
            this.closeTooltip(tooltip.id)
        }, duration)
    },
    closeTooltip(tooltipId) {
        var mR = $('#' + tooltipId).css('width')
        $('#' + tooltipId).animate({
            marginRight: '-=' + mR,
            opacity: '-=1'
        }, {
            duration: 300,
            complete: function() {
                $(this).remove()
            }
        })
    },
    getCart: function(product_id) {
        var cart = (JSON.parse(localStorage.getItem('cart')) || []);
        if (typeof product_id !== 'undefined') {
            var item = cart.filter(function(i) {
                if (typeof i !== 'undefined')
                    return i.id == product_id;
            })
            if (item.length == 1)
                return item[0];
        } else
            return cart;
    },
    clearCart: function() {
        localStorage.setItem('cart', '[]')
    },
    toCart: function(product_id, newCount) {
        /*
        var cart = (JSON.parse(localStorage.getItem('cart')) || [])
        if (cart.filter(function(i) {
                if (typeof i !== 'undefined')
                    return i.id == product_id
            }).length > 0) {
            cart = cart.map(function(i) {
                if (typeof i !== 'undefined')
                    i.count = (newCount || i.count + 1)
                return (i || null);
            })
        } else {
            cart.push({
                id: product_id,
                count: (newCount || 1)
            })
        }
        try {
            localStorage.setItem('cart', JSON.stringify(cart));
        } catch (e) {
            if (e == QUOTA_EXCEEDED_ERR) {
                this.tooltip('Слишком большое количество товаров в корзине!')
            }
        }
        this.tooltip('Пластинка успешно добавлена в корзину!')*/
        $.post("/cart/addproduct" + queryParser.stringify({product: product_id, count: (newCount || 1)}),
        (data, status) => {
            var d = JSON.parse(data)
            this.tooltip(d.message)
            var sel = $('#counter' + product_id)
            if(typeof sel !== 'undefined' && d.success) {
                var t = sel.text().match(/\d+/)[0]
                sel.text((+t + newCount) + ' шт.')
                $('#fullprice').text(d.cost + ' ₽')
            }
        });

    },
    fromCart: function(product_id, newCount) {
        /*var cart = (JSON.parse(localStorage.getItem('cart')) || []);
        localStorage.setItem('cart', JSON.stringify(cart.filter(function(i) {
            return i.id !== product_id;
        })));*/
        $.post("/cart/removeproduct" + queryParser.stringify({product: product_id, count: (newCount || 'all')}),
        (data, status) => {
            var d = JSON.parse(data)
            this.tooltip(d.message)
            if(d.success) {
                if(product_id === 'all') {
                    $('.cart-product').remove()
                    $('#fullprice').text('0 ₽')
                }
                else {
                    var t = $('#counter' + product_id).text().match(/\d+/)[0]
                    var nC = t - newCount
                    if(nC <= 0 || isNaN(nC))
                        $('#product' + product_id).remove()
                    else 
                        $('#counter' + product_id).text(nC + ' шт.')
                }
                $('#fullprice').text(d.cost + ' ₽')
            }
        });
    },
    changeRating(product_id, new_rating, logged_in) {
        $.post("/products/rate" + queryParser.stringify({product: product_id, mark: new_rating}),
        (data, status) => {
            var d = JSON.parse(data)
            this.tooltip(d.message)
            if(d.rate == 0) return;
            $('#star-' + Math.round(d.rate) + '-' + product_id).prop('checked', true)
            for(var i = 1; i <= 5; i++) 
                $('#star-' + i + '-' + product_id).prop('disabled', true)
        });
    }
}

$(document).ready(function() {
    $('#genres-list > ul li').click()
    $('*').tooltip('enable')    // Bug in bootstrap?
    App.init()
});