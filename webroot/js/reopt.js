//
// Оптимизация регулярных выражений.
// Портировано на javascript c java-кода опубликованного
// в статье http://habrahabr.ru/post/117177/
// В коде оставлены оригинальные комментарии автора.
// https://habrahabr.ru/users/yuk/
//

(function () {

  var START = '\x00';
  var END = '\x01';

  var RexExpNode = function () {
    this.ch = START;
    this.nodes = [];
  }

  RexExpNode.prototype = {

    add: function (str) {
      if (str.length === 0) return;
      var chNew = str.charAt(0);
      for (var i = 0, len = this.nodes.length; i < len; i++) {
        var n = this.nodes[i];
        if (n.ch === chNew) {
          n.add(str.substring(1));
          return;
        }
      }
      var newNode = new RexExpNode();
      newNode.ch = chNew;
      newNode.add(str.substring(1));
      this.nodes.push(newNode);
    },

    // Как мы видим его основной метод add проверяет есть ли первый символ среди его детей,
    // если нет, то создает и отдает тому поддереву, которое начинается с этого символа.
    // Таким образом в данной структуре любой префикс хранится только один раз (путь по дереву)
    // и переиспользуется когда встречается в наших строках.
    // Второй метод конвертирует дерево в регулярное выражение.

    toRegExp: function () {
      var str = [];
      if (this.ch === START) {

      } else if (this.ch === END) {

      } else {
        // convert special characters like {}[].
        var newStr = escre(this.ch);
        str.push(newStr);
      }
      if (this.nodes.length > 1) {
        str.push('(?:');
        for (var i = 0, len = this.nodes.length; i < len; i++) {
          var n = this.nodes[i];
          str.push('');
          str.push(n.toRegExp());
          str.push('|');
        }
        str.length = str.length - 1;
        str.push(')');
      } else if (this.nodes.length === 1) {
        str.push(this.nodes[0].toRegExp());
      }
      return str.join('');
    }
  };

  function escre (re) {
    return re.replace(/[\*\!\?\:\[\]\(\)\{\}\.\|]/g, '\\$&');
  }

  // Вот рабочий код
  RegExp.newFromList = function (strs, finalize) {
    strs.sort(function (a, b) {
      return a != b ? (a > b ? 1 : -1) : 0
    });
    var root = new RexExpNode();
    for (var i = 0, len = strs.length; i < len; i++) {
      var str = strs[i];
      root.add(str + '$');
    }
    return root.toRegExp();
  };

})();