'use strict';

var queryParser = {
    parse: function(query = window.location.search, hash = '#') {
        var result = '{';
        decodeURI(query).replace('?', '').replace(hash, '').split('&').map(function(i) {
            return i.split('=');
        }).forEach(function(i) {
            if(typeof i[1] !== 'undefined') {
                var o = {}
                o[i[0]] = i[1]
                o = JSON.stringify(o)
                o.substr(1, o.length - 2)
                result += o.substr(1, o.length - 2) + ',';
            }
        });
        if(result.length == 1)
            return {}
        else
            return JSON.parse(result.substr(0, result.length - 1) + '}')
    },
    arg: function(argument, data = null, query = window.location.search, hash = '#') {
        return this.parse()[(argument || '')]
    },
    stringify: function(query) {
        if (typeof query !== 'object' || query == 'null')
            return undefined;
        return encodeURI('?' + Object.keys(query).map(function(i) {
            if(query[i].toString().length > 0)
                return i + '=' + query[i]
        }).join('&')).replace(/\?\&+/,'?').replace(/\&{2,}/g,'&').replace(/\&+$/,'')
    }
}

if (typeof module !== 'undefined') module.exports = queryParser;