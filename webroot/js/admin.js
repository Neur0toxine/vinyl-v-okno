var App = {
	cropper: null,
	imageBlob: null,
	init: function() {
		$('input').tooltip('enable')	// Bug in bootstrap ?
		if(/\/products\/edit.*/i.test(window.location.pathname)) {
			this.inputToSelect('#authors_fallback', '#authors')
			this.inputToSelect('#tracks_fallback', '#tracks')
		}
		if(/\/orders\/edit.*/i.test(window.location.pathname)) this.parseOrder()
		if(/\/cart\/edit.*/i.test(window.location.pathname)) this.parseCart()
		var img = $('#picField')
		if(typeof img !== 'undefined')
			img.on('change', this.parseProductImg)
		console.log('admin app v0.1')
	},
	parseOrder: function(){
		$('#order_fallback').html('');
		var data = JSON.parse($('input[name="contents"]').val())
		data.data.forEach(function(i){
			$('#order_fallback').append('<option>Пластинка №' + i.id + ', ' + i.count +
			' штук, ' + i.price + ' руб/шт.</option>')
		})
		$('#itog').html('<b>Итого:</b> ' + data.costs + ' руб.');
	},
	parseCart: function(){
		$('#cart_fallback').html('');
		var data = JSON.parse($('input[name="contents"]').val())
		data.forEach(function(i){
			$('#cart_fallback').append('<option>Пластинка №' + i.id + ', ' + i.count + ' шт.</option>')
		})
	},
	jsonFromOrderItemText: function(){
		var cg = $('#order_fallback').val()[0].match(/(\d+)/g)
		if(Array.isArray(cg) && cg.length == 3) {
			$('#order_fallback_text').val(cg[0])
			$('#order_cnt').val(cg[1])
			$('#order_price').val(cg[2])
		}
	},
	jsonFromCartItemText: function(){
		var cg = $('#cart_fallback').val()[0].match(/(\d+)/g)
		if(Array.isArray(cg) && cg.length == 2) {
			$('#cart_fallback_text').val(cg[0])
			$('#cart_cnt').val(cg[1])
		}
	},
	addToOrder: function(e) {
		if(typeof e !== 'undefined')
			e.preventDefault()
		var data = JSON.parse($('input[name="contents"]').val())
		data.data.push({
			id: $('#order_fallback_text').val(),
			price: $('#order_price').val(),
			count: $('#order_cnt').val()
		})
		data.costs += $('#order_cnt').val() * $('#order_price').val()
		$('input[name="contents"]').val(JSON.stringify(data))
		this.parseOrder()
	},
	addToCart: function(e) {
		if(typeof e !== 'undefined')
			e.preventDefault()
		var data = JSON.parse($('input[name="contents"]').val())
		data.push({
			id: $('#cart_fallback_text').val(),
			count: $('#cart_cnt').val()
		})
		data.costs += $('#cart_cnt').val() * $('#cart_price').val()
		$('input[name="contents"]').val(JSON.stringify(data))
		this.parseCart()
	},
	removeFromOrder: function(e){
		if(typeof e !== 'undefined')
			e.preventDefault()
		var fullprice = 0
		var data = JSON.parse($('input[name="contents"]').val())
		var dt = data.data.filter(function(i){
			return i.id != $('#order_fallback_text').val();
		})
		dt.forEach(function(i){
			fullprice += Number.parseFloat(i.price)
		})
		data.data = dt
		data.costs = fullprice
		$('input[name="contents"]').val(JSON.stringify(data))
		this.parseOrder()
	},
	removeFromCart: function(e){
		if(typeof e !== 'undefined')
			e.preventDefault()
		var data = JSON.parse($('input[name="contents"]').val())
		var dt = data.filter(function(i){
			return i.id != $('#cart_fallback_text').val();
		})
		data = dt
		$('input[name="contents"]').val(JSON.stringify(data))
		this.parseCart()
	},
	addToList: function(selText, selList, e) {
		if(typeof e !== 'undefined')
			e.preventDefault()
		var text = $(selText).val()
		if($(selList + " > option:contains('" + text + "')") !== 'object')
			$(selList).append('<option>' + text + '</option>')
	},
	removeFromList: function(selText, selList, e) {
		if(typeof e !== 'undefined')
			e.preventDefault()
		$(selList + " > option").each(function(i,j){
			if($(j).text() == $(selText).val())
				$(j).remove()
		})
	},
	textFromSelected: function(list, input) {
		var val = $(list).val();
		if(Array.isArray(val) && val.length >= 1)
			$(input).val(val[0]);
	},
 	selectToInput(sel, inp) {
 		var obj = $(sel + ' > option')
 		var arr = []
 		obj.each(function(i,j){
 			arr.push($(j).text())
 		})
 		$(inp).val(JSON.stringify(arr))
 	},
 	inputToSelect(sel, inp) {
 		var select = $(sel)
 		var input = $(inp)
 		if(typeof select === 'undefined' || typeof input === 'undefined')
 			return false
 		var obj = JSON.parse(input.val())
 		if(Array.isArray(obj))
 			obj.forEach(function(i){
 				select.append('<option>' + i + '</option>')
 			})
 	},
 	storeProductFallbacks: function() {
 		this.selectToInput('#authors_fallback', '#authors')
		this.selectToInput('#tracks_fallback', '#tracks')
 	},
    generateRandomString: function(strlength = 20) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < strlength; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    },
    tooltip: function(text, duration = 2000, closable = true, bgcolor = '', textcolor = '', ttWidth = '27rem') {
        var tooltip = document.createElement('div')
        $(tooltip).attr('class', 'app-tooltip')
        $(tooltip).attr('id', 'tooltip-' + this.generateRandomString())
        $(tooltip).css('margin-top', '2px')
        $(tooltip).css('margin-right', '-' + (ttWidth || '27rem'))
        $(tooltip).css('width', (ttWidth || '27rem'))
        if (bgcolor.length > 0) $(tooltip).css('background', bgcolor)
        if (textcolor.length > 0) $(tooltip).css('color', textcolor)
        $(tooltip).append('<span>' + text + '</span>')

        if (closable) {
            var tooltipClose = document.createElement('div')
            var tooltipIcon = document.createElement('i')
            $(tooltipIcon).attr('class', 'fa fa-times')
            $(tooltipIcon).attr('aria-hidden', 'true')
            $(tooltipIcon).css('border-left', '1px dotted ' + (textcolor || 'white'))
            $(tooltipClose).attr('class', 'app-tooltip-close')
            $(tooltipClose).attr('onclick', 'App.closeTooltip(\'' + tooltip.id + '\')')
            $(tooltipClose).append(tooltipIcon)
            $(tooltip).append(tooltipClose)
        }

        $('#tooltips-container').prepend(tooltip);

        $(tooltip).animate({
            marginRight: '0rem',
            opacity: '+=1'
        }, {
            duration: 300,
        });

        if (duration > 0) setTimeout(() => {
            this.closeTooltip(tooltip.id)
        }, duration)
    },
    closeTooltip(tooltipId) {
        var mR = $('#' + tooltipId).css('width')
        $('#' + tooltipId).animate({
            marginRight: '-=' + mR,
            opacity: '-=1'
        }, {
            duration: 300,
            complete: function() {
                $(this).remove()
            }
        })
    },
 	sendProductForm: function(e) {
 		if(typeof e === 'undefined')
 			return false
 		e.preventDefault()
 		this.storeProductFallbacks()

 		if($('.progress.hidden').length == 0) {
 			this.tooltip('Уже идёт отправка, будьте терпеливы...')
 			return false
 		}

 		if($(productsform.name).val() == '' || $(productsform.name).val().length <= 3) {
 			this.tooltip('Укажите имя (длиннее трёх букв)')
 			return false
 		}

 		if($(productsform.year).val() == '') {
 			this.tooltip('Укажите год выпуска')
 			return false
 		}
 		if($(productsform.rating).val() == '') {
 			this.tooltip('Укажите рейтинг')
 			return false
 		}
 		if($(productsform.rating_count).val() == '') {
 			this.tooltip('Укажите количество оценок')
 			return false
 		}
 		if($(productsform.authors).val() == '') {
 			this.tooltip('Укажите авторов (композиторов?)')
 			return false
 		}
 		if($(productsform.tracks).val() == '') {
 			this.tooltip('Укажите композиции')
 			return false
 		}
 		if($(productsform.price).val() == '') {
 			this.tooltip('Укажите стоимость')
 			return false
 		}
 		if($(productsform.bought).val() == '') {
 			this.tooltip('Укажите количество покупок')
 			return false
 		}
 		if($('select#categories-ids').val() == []) {
 			this.tooltip('Выберите хотя бы один жанр')
 			return false
 		}

 		var fd = new FormData(productsform)
 		if(this.imageBlob != null)
 			fd.append('photo', this.imageBlob)

 		$('.progress').removeClass('hidden')
		var xhr = new XMLHttpRequest()
	    xhr.open( 'POST', productsform.action, true );
	    xhr.onreadystatechange = function ( response ) { 
		    if (this.readyState === this.DONE) {
		    	$('.progress').addClass('hidden')
		        window.location.assign(this.responseURL)
		    }
	    };
	    xhr.send(fd);
 	},
 	parseProductImg: function (evt) {
 		if(!$.isEmptyObject(App.cropper)) {
 			App.cropper.destroy()
 		}
			    var tgt = evt.target || window.event.srcElement,
			        files = tgt.files;
			    if (FileReader && files && files.length) {
			        var fr = new FileReader();
			        fr.onload = function () {
			            $('#product-image').attr('src', fr.result)
			            App.cropper = new Cropper($('#product-image')[0], {
						  aspectRatio: 4 / 4,
						  viewMode: 2,
						  minCanvasWidth: 300,
						  minCanvasHeight: 300,
						  minContainerWidht: 300,
						  minContainerHeight: 300
						});
			        }
			        fr.readAsDataURL(files[0]);
			    }

			    // Not supported
			    else {
			        // fallback -- perhaps submit the input to an iframe and temporarily store
			        // them on the server until the user's session ends.
			    }
	},
	acceptImg: function(e){
		if(typeof e !== 'undefined')
			e.preventDefault()
		var lgImg = $('#selectedImageBig')
		var smImg = $('#selectedImageSmall')
		lgImg.attr('src', 
			this.cropper.getCroppedCanvas({
			  minWidth: 300,
			  minHeight: 300,
			  maxWidth: 4096,
			  maxHeight: 4096,
			  fillColor: '#fff',
			  imageSmoothingEnabled: false,
			  imageSmoothingQuality: 'high',
			}).toDataURL('image/jpeg')
		)
		smImg.attr('src', 
			this.cropper.getCroppedCanvas({
			  minWidth: 152,
			  minHeight: 152,
			  maxWidth: 4096,
			  maxHeight: 4096,
			  fillColor: '#fff',
			  imageSmoothingEnabled: false,
			  imageSmoothingQuality: 'high',
			}).toDataURL('image/jpeg')
		)
		this.cropper.getCroppedCanvas({
			  minWidth: 300,
			  minHeight: 300,
			  maxWidth: 4096,
			  maxHeight: 4096,
			  fillColor: '#fff',
			  imageSmoothingEnabled: false,
			  imageSmoothingQuality: 'high',
			}).toBlob((blob) => {
				this.imageBlob = blob;
			})
	}
}

$(document).ready(function(){ 
	App.init() 
	/*
	 * Строчку кода ниже писал холоп Венька.
	 * Писал с леностию дрянно, за что был бит плетьми.
	*/
	$('body').append('<style>form[name^="post_"][style="display:none;"] + a.btn{border-top-left-radius:.2rem!important;border-bottom-left-radius:.2rem!important}</style>')
})